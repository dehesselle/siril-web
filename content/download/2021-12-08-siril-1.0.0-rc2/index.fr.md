---
title: Siril 1.0.0-rc2
author: Cyril Richard
date: 2021-12-08T00:00:00+01:00
categories:
  - Nouvelles
tags:
  - new release
aliases:
  - /fr/download/1.0.0-rc2/
version: "1.0.0-rc2"
linux_appimage: "https://free-astro.org/download/Siril-1.0.0-rc2-x86_64.AppImage"
linux_binary_32: ""
linux_binary_64: ""
windows_binary: "https://free-astro.org/download/siril-1.0.0-rc2-setup.exe"
windows_xp_binary: ""
mac_binary: "https://free-astro.org/download/siril-1.0.0-rc2-x86_64.dmg"
source_code: "https://free-astro.org/download/siril-1.0.0-rc2.tar.bz2"
---

Cette version est une stabilisation de la version rc1 dont les nouveautés sont présentées en détail [ici](../2021-11-20-siril-1.0.0-rc1). Nous vous invitons à lire attentivement cette page afin de connaitre les fonctions implémentées. Cependant, nous avons tourné une courte vidéo expliquant l'utilisation des améliorations du graphique :

<video width="100%" controls>
  <source src="https://free-astro.org/videos/Plot_video.fr.webm" type="video/webm">
Your browser does not support the video tag.
</video>


### Téléchargements
Siril 1.0.0-rc2 est distribué pour les 3 plates-formes les plus courantes (Windows, MacOS, GNU / Linux). Voir la page [téléchargement](../../../fr/download).

Mais bien sûr, puisque Siril est un logiciel libre, il peut être construit sur n'importe quel OS à partir des sources, voir la page [installation](https://free-astro.org/index.php?title=Siril:install/fr).

### Contributeurs
Les contributeurs à cette release sont : Cyril Richard, Cécile Melis, Vincent Hourdin, Matthias Glaub.

### Traducteurs
Cyril Richard, Stefan Beck, Kostas Stavropoulos, Xiaolei Wu, Joaquin Manuel, Alexandre Prokoudine, Luca Benassi, Antoine Hlmn, Kenji Iohara, Marta Fernandes

### Dons
Développer des logiciels est amusant, mais cela nous prend aussi presque tout notre temps libre. Si vous aimez Siril et que vous souhaitez nous soutenir dans la poursuite de son développement, vous pouvez faire un [don](../../donate) d’une somme de votre choix.

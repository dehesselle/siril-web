---
title: Siril 0.99.10.1
author: Cyril Richard
date: 2021-06-23T01:00:00+02:00
categories:
  - News
tags:
  - new release
version: "0.99.10.1"
linux_appimage: "https://free-astro.org/download/Siril-0.99.10.1-x86_64.AppImage"
linux_binary_32: ""
linux_binary_64: ""
windows_binary: "https://free-astro.org/download/siril-0.99.10.1-setup.exe"
windows_xp_binary: ""
mac_binary: "https://free-astro.org/download/siril-0.99.10.1-x86_64-1.dmg"
source_code: "https://free-astro.org/download/siril-0.99.10.1.tar.bz2"
---

### <span style="color:red">June 23rd, release udpate</span>

Due to some bug reports with 0.99.10 version, we've decided to release this patched version.

* Fixed star detection with resolution < 1.0
* Fixed interpolation issue in global registration
* Fixed timestamp issue with glib < 2.66
* New MAD clipping algorithm

### The version
This is the fourth beta of the upcoming version 1.0. It comes with some new features and many bug fixes compared to the previous beta version (0.99.8.1) and also compared to the stable version (0.9.12), and continues the big refactoring started with the 0.99 series.

The most important changes are concentrated on the stacking step which has undergone many improvements, and we would like to thank Cécile Melis for her work and her valuable analysis on this new version.

### Downloads
As a beta version of Siril 1.0, we only distribute this version with the packages for the 3 most common platforms (Windows, MacOS, GNU/Linux). See the [download](../../../download) page.

But as usual, since siril is a free software, it can be built on any OS from source, see the installation page.

## What is new in this release?

### New alignment algorithm on 2 or 3 stars
Sometimes, especially with the combined use of a long focal length and very short exposures, like in lucky imaging for example, there are very few stars visible in the field. For Global Alignment to work, you need a minimum of 10 stars and sometimes that is too much. A new registration method has therefore been implemented and operates semi-automatically. Indeed, one needs to only select 2 or 3 stars to be able to align the whole sequence. Having 2 or 3 stars enables to take into account the field rotation, and therefore, a new sequence is created.

{{<figure src="3stars.png" link="3stars.png" width="100%">}}

### New pixel rejection algorithms
#### Generalized Extreme Studentized Deviate Test
The `Generalized Extreme Studentized Deviate Test` algorithm is a generalization of the Grubbs test which is used to detect one or more outliers in a univariate data sample that follows an approximately normal distribution. In other words, this algorithm assumes that the pixel stack follows a Gaussian distribution. This algorithm shows excellent performance with a large data set of more than 50 images. In addition, it has the great advantage of no longer having to specify both high and low sigma rejection values. Although two other parameters exist, their default values are usually sufficient and no further adjustment is required.

{{<figure src="GESDT.png">}}

#### k-MAD clipping
This algorithm was added in the 0.99.10.1 update. It is an iterative algorithm working as Sigma Clipping except that the estimator used is the Median Absolute Deviation (MAD). This is generally used for noisy infrared image processing.

### Added weighting of images according to noise
Until now, Siril stacked the images on the assumption that all images contribute equally to the final image. In fact, this does not generally lead to an optimal combination in terms of improving the Signal/Noise ratio. Indeed, suppose that one of the combined images has more noise than the others. If we just stack this image then there is a good chance that it will degrade the result a bit as some of the noise will be treated as if it were a signal.

In order to maximize the Signal/Noise ratio in the final image, it is now possible to assign a multiplicative weighting factor to each input image. The image weights should reflect as accurately as possible the existing `Signal/Noise` differences in the original data set in order to give good results. However, the improvement being generally quite light, the major contribution resulting from the normalization, this option is not checked by default and by design, it can only be activated if the normalization is on too.

{{<figure src="weighted.png">}}

### Normalization refactoring
Previous versions of Siril normalized images from data calculated on the reference channel. By default, it was green but it could be changed. Although the results obtained were quite satisfactory, it turned out that in some cases, it was not at all optimal. The choice was therefore made to calculate the normalization on the 3 RGB channels for the color images. The counterpart of this change should have been a calculation time 3 times longer. However, we have also changed the algorithm for calculating the normalization which runs 3 times faster than before. In summary, the normalization speed is approximately the same on color images, but 3 times faster on monochrome images.

{{<figure src="normalization.png" link="normalization.png" caption="The levels of the channels of 4 sessions made under different sky conditions (presence of the moon) are represented before and after the normalization. On the left, this is version 0.99.8.1, to the right of 0.9.10. We can see that the normalization is much more robust in the new version." width="100%">}}

### Remove black borders of stacked image
Version 0.99.10 puts an end to one of Siril's most annoying artifacts, the black bands at the edge of images present after stacking. These black bands were often a source of problems, especially before the extraction of the light pollution gradient where we recommended cropping the image in order to exclude them before each process. Today, even if these bands no longer exist, a cropping of the final image may still be necessary, the borders remaining increasingly noisy.
{{<figure src="border_comp.png" link="border_comp.png" caption="The stacking results are now free of black bands that could be present when tracking is not perfect or in case of imperfect overlap between multiple sessions." width="100% ">}}

### Possibility of processing with synthetic bias
This new version offers the possibility of calibrating the images with a synthetic bias in order to minimize the introduction of noise. This functionality is fully explained in this [tutorial](../../tutorials/synthetic-biases/).

### Extraction of the green channel
In order to improve the photometric measurements made with color sensors - see the tutorial on [photometry](../../tutorials/photometry/) - the new commands `extract_Green` and` seqextract_Green` allow to extract the 2 green pixels of the Bayer pattern and to add them. This operation increases the signal/noise ratio at the expense of a loss of resolution, but this is not essential for flux measurements.

### Update sampling after channel extraction and/or Drizzle
Until version 0.99.8.1, when doing a `CFA`,` Ha` or `OIII` channel extraction, the header information of the FITS file was lost. With 0.99.10, these values are kept and updated. For example, if the pixel size is entered, it will be multiplied by 2 during a CFA channel extraction. Also, if the user stacks images with Drizzle option, the size of the pixels is this time divided by 2. These operations are therefore completely transparent with the scripts `Extract_Ha` or` Extract_HaOIII` because the value of the pixels is multiplied by 2 then divided by 2 in order to get the initial value back. However, this change may be important when doing normal processing with Drizzle. No need now to artificially multiply the focal length by 2 to perform astrometry. If despite everything, the astrometric resolution fails on the drizzled image, a new option has been added to help with such difficult cases. This is the "Downsample image" option. The image will be reduced internally by the algorithm, but your image remains unchanged.

{{<figure src="downsample.png">}}

### Keep WCS info in stacked image
More and more astrophotographers are using on-board solutions that record WCS coordinates in images. These coordinates are essential for performing astrometric resolution. Consequently, and to simplify the latter, the WCS coordinates of the reference image are now transferred to the header of the stacked image. They are therefore imported, along with the focal length and pixel size, by pressing the `Get Image Metadata` button.

### Added drag and drop to open an image, or a sequence
It is now possible to drag into Siril's image area either an image (in supported format) or a `.seq` file.

{{<figure src="drag_and_drop.png" width="100%">}}

## Performance tests
With all the profound changes in fundamental Siril algorithms, we decided to perform performance tests comparing this version and the previous version, 0.99.8.1. The tests were carried out on the two types of existing FITS files (regular FITS and FITS sequence), for RGB images with demosaicing at first.

A classic processing is carried out on the 2 types of files, in 16 bits and in 32 bits, which includes the following steps:
- raw preprocessing with a masterdark and a masterflat (preprocess)
- global alignment (register)
- normalization then stacking with rejection (stack)

The results are shown in the figure below:

{{<figure src="PerfColor.png" link="PerfColor.png" width="100%" caption="Processing time comparison between versions 0.99.8.1 and 0.99.10 for color images. This latest version is faster when processing is done in 32bits.">}}

In 16 bits, the processing time is more or less the same, although a little longer with this new version. However, in 32 bits the advantage clearly goes to version 0.99.10.

The same test is then carried out on monochrome images and the results are shown below. In addition to the FITS and FITSEQ formats, the SER format is also added to the comparison. Indeed, this comparison was not possible with 0.99.8.1 for color images, since the demosaicing of a SER at the preprocessing stage was not allowed (this is now possible with 0.99.10 ).

{{<figure src="PerfMono.png" link="PerfMono.png" width="100%" caption="Comparison of processing times between versions 0.99.8.1 and 0.99.10 for monochrome images. This latest version is faster regardless of format and bit depth.">}}

### Translators
Cyril Richard, Stefan Beck, Kostas Stavropoulos, Xiaolei Wu, Joaquin Manuel, Alexandre Prokoudine, Luca Benassi, Antoine Hlmn, Kenji Iohara, Marta Fernandes

### Donate 
Developing software is fun, but it also takes up nearly all of our spare time. If you like Siril and would like to support us in continuing development you’re welcome to donate a small amount of your choice.

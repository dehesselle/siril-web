---
title: Siril 1.0.5
author: Cyril Richard
date: 2022-09-09T01:00:00+00:00
categories:
  - Nouvelles
tags:
  - new release
aliases:
  - /fr/download/1.0.5/
version: "1.0.5"
linux_appimage: "https://free-astro.org/download/Siril-1.0.5-x86_64.AppImage"
linux_binary_32: ""
linux_binary_64: ""
windows_binary: "https://free-astro.org/download/siril-1.0.5-setup.exe"
windows_xp_binary: ""
mac_binary: "https://free-astro.org/download/siril-1.0.5-x86_64.dmg"
source_code: "https://free-astro.org/download/siril-1.0.5.tar.bz2"
---

Peu après la sortie de la version 1.0.4, la semaine dernière, nous sommes obligé d'apporter un correctif et de sortir Siril 1.0.5 bien plus tôt que prévu. En effet, nous avons déniché un bug introduit lors de la version précédente alors que nous avions corrigé un bug. Parfois, les erreurs se compensent et ne se voient pas, mais lorsque l'on en corrige une, l'autre se manifeste alors. C'est exactement ce qui s'est passé ici. Or, ce bug est critique et provoque un décalage de 1 pixel en x et y après un alignement d'images sur lesquelles il y a eu un retournement au méridien. Nous tenons a chaleureusement remercier Kristopher Setnes pour avoir trouvé ce bug et permis que ce dernier ne reste pas longtemps en liberté.

### Téléchargements
Siril 1.0.5 est distribué comme d'habitude pour les 3 plates-formes les plus courantes (Windows, MacOS, GNU / Linux). Voir la page [téléchargement](../../../fr/download).

Mais bien sûr, puisque Siril est un logiciel libre, il peut être construit sur n'importe quel OS à partir des sources, voir la page [installation](https://free-astro.org/index.php?title=Siril:install/fr).

### Quelles sont les nouveautés
Évidemment les nouveautés sont faibles, mais pas inexistantes. En effet, un nouveau bouton voit le jour dans l'extraction de gradient : il permet, lorsqu'on le laisse pressé, de voir l'image originale afin de comparer avant/après.
La liste des bugs corrigés est la suivante :
- le bouton pipette de l'outil d'étirement hyperbolique ne marchait pas sur les images 16bits
- la lecture des catalogues GAIA avait un problème et pouvait provoquer l'échec de l'astrométrie
- l'alignement avait un décalage de 1px en x et y sur les images retournées après le méridien

### Siril dans le Windows Store
Enfin, cette nouvelle ne serait pas complète si je ne vous annonçais pas la grande nouvelle de la présence officielle de Siril dans le [store de Microsoft](https://apps.microsoft.com/store/detail/siril/XPDM23ZQ9CCLVF). Ceci est en effet un réel progrès en terme de visibilité et une sorte de reconnaissance : n'hesitez pas à laisser une évaluation de l'application. Bien sûr nous continuerons à produire un installeur classique pour les utilisateurs qui le préfèrent.

À l'heure où nous écrivons ces lignes, la version 1.0.4 est disponible au téléchargement. Alors bien sûr, lors d'une nouvelle version de Siril il faut attendre quelques heures avant de pouvoir la télécharger via le store, et nous n'avons pas le contrôle la dessus.

{{<figure src="MicrosoftStore.fr.png" link="MicrosoftStore.png" width="100%" caption="Siril dans le Store Microsoft. N'hesitez pas à laisser une évaluation.">}}

Bien sûr, Siril est un logiciel informatique développé par des humains faillibles, et des bugs peuvent encore exister (je crois que vous pouvez vous en rendre compte avec cette sortie prématurée). Si vous pensez en découvrir, merci de nous [contacter](../../faq/#comment-puis-je-contacter-léquipe-de-siril-) ou d'[écrire](https://gitlab.com/free-astro/siril/-/issues/new?issue%5Bmilestone_id%5D=) un rapport de bug si ce dernier n'est pas déjà présent dans la [liste des bugs connus](https://gitlab.com/free-astro/siril/-/issues?sort=updated_desc&state=opened).


Les **contributeurs** de cette version sont : Cyril Richard, Cécile Melis, Vincent Hourdin. Nous tenons également à remercier tous les bêta-testeurs et notamment Fred Denjean, pour son don à trouver les bugs.

### Dons
Développer des logiciels est amusant, mais cela nous prend aussi presque tout notre temps libre. Si vous aimez Siril et que vous souhaitez nous soutenir dans la poursuite de son développement, vous pouvez faire un [don](../../donate) d’une somme de votre choix.

---
title: Animation of comet Neowise
author: Argonothe
date: 2020-07-30T07:49:37+00:00
featured_image: NeoWise-Abendhimmel-534x800px.gif
categories:
  - News
tags:
  - NeoWise
---

Comet NeoWise in the evening sky – above **Brünstelkreuz mountain** (1734m)

![Comet Neowise Animation](NeoWise-Abendhimmel-534x800px.gif)

The author: Frank Stefani is a beginner in astrophotography (since 6 months),and also been a professional photographer and experienced visual-only astronomer for over 4 decades.

Bravo, I love this picture!

Source: https://frank-stefani.de/astronomy/C2020F3NeoWise/C2020F3-NeoWise-Abendhimmel.html

https://discuss.pixls.us/t/how-to-stack-frozen-tracked-neowise-over-moving-foreground/19462

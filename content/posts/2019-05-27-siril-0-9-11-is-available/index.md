---
title: Siril 0.9.11 is available
author: Cyril Richard
date: 2019-05-27T09:39:31+00:00
featured_image: /wp-content/uploads/2019/05/about.png
categories:
  - News
---

We are glad and proud to announce the release of the new Siril version: 0.9.11. This version containing a lot of improvement is accessible through the download section.

For more information about the new features, please follow this [link][1].

 [1]: https://free-astro.org/index.php?title=Siril:0.9.11

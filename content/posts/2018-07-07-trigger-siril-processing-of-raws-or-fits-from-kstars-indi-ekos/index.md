---
title: Starts SIRIL pre-processing from KSTARS / INDI / EKOS
author: Argonothe
date: 2018-07-07T12:17:11+00:00
featured_image: /wp-content/uploads/2018/07/siril-ekos.png
categories:
  - Tips
---

You may want to use Siril scripts to automatically pre-process your images at the end of the nightly sequence with KSTARS / INDI / EKOS. Or, you also may want to download the pre-processed, registered and stacked file and not all the RAW files in the case of a remote control observatory ?

Below is described the detailed procedure that was established after an exchange with Jasem, the main developer of <a href="http://www.indilib.org/about/ekos.html" target="_blank" rel="noopener">EKOS</a> :

## Procedure

  1. Adapt the processing <a href="https://free-astro.org/siril_doc-en/#scripting" target="_blank" rel="noopener">script for SIRIL</a> according to your configuration (EKOS saving directory and file settings) use the <strong><em>-flip</em></strong> option of the pre-process command in case you use KSTARS FITS (see the available commands <a href="https://free-astro.org/siril_doc-en/#Available_commands" target="_blank" rel="noopener">here</a>) 
  2. Create a bash script 
     ```bash
     #!/bin/bash
     # Version 1.0
     # Trigger Siril processing
     
     siril -s ~ /.siril/scripts/DSLR_preprocessing.ssf -d ~ /Images/siril/Bulle/Script
     ```

     Where `~/.siril/scripts/` is the path, `DSLR_preprocessing.ssf` the script name and `/Images/siril/Bulle/Script` the working directory<em> where the images are stored.</em> </li> 
      * Save this script by naming it <strong>siril.sh</strong> for example<strong><br /> </strong>
      * Make it executable 
        `$ chmod +x siril.sh`
      * Then, in Ekos, use the Sheduler (cf. screenshot)<br /> 
      * Select your script <strong>siril.sh</strong> as in the screenshot above
      * The script will be executed after the scheduled shooting</ol>

---
title: What’s new in Siril 0.99.4
author: Cyril Richard
date: 2020-09-02T11:04:30+00:00
featured_image: comparison.png
categories:
  - News
---


Siril 0.99.4, available [here](https://www.siril.org/2020/08/13/siril-1-0-0beta/), has introduced a lot of new features. In this post, we have listed the highlighted ones.

## 32-bit native

The most important and especially the most awaited change is the switch to native 32-bit floating point for all calculations. The default FITS format is now of type TFLOAT. The files are therefore twice as large in size as before and the memory used is also twice as large. However, where one would have expected twice as long computation times, a lot of optimization work was done throughout the code. For comparison, on a machine with an I9 core, 32GB of DDR4 RAM (a good machine), a stack of 47 images takes 5min 24s with version 0.9.12 while only 2min01s are needed to stack 32bits images under 0.99.4\. **That makes Siril probably the fastest tool to process your astronomy images.**

## New demosaicing algorithms.

With the introduction of 32-bit floating point in Siril, the need to completely review our demosaicing algorithms quickly became obvious. Indeed, these worked only in 16-bit.

9 new demosaicing algorithms have therefore been implemented in Siril. Of course, a user will rarely need to change this settings and therefore, choosing the default algorithm has been a crucial step. Our choice was done on the **RCD algorithm** : a very powerful algorithm for circular objects, such as stars. It produces images of the same quality as other software that uses the Bayer Drizzle technique. It replaces the VNG algorithm which was the default in Siril until version 0.9.12.


{{< figure src="comparison.png" link="comparison.png" caption="Comparison" >}}


In this illustration (Siril / APP Comparison) Siril’s RCD algorithm is compared to different Bayer Drizzle results obtained with APP over a hundred images. Only the Bayer-Drizzle Point Kernel appears to have significantly higher resolution, however, this comes at the cost of a large introduction of noise that is generally not desirable and of course higher processing time.


## Removed artifacts from X-TRANS sensors

These Fuji camera sensors are known to have artifacts when taking Bias or Darks images. Indeed, an uggly square pattern is visible in the image. An algorithm has therefore been introduced in order to remove this artifact. Either automatically during preprocessing, or from the command line before preprocessing.

{{< figure src="Xtrans_1_2.jpg" link="Xtrans_1_2.jpg" caption="Xtrans_1_2" >}}


## FITS compression

It is now possible to work with compressed FITS. Compression can be lossy or lossless. Although the first option may scare the user, it is very difficult (impossible?) to notice a difference between an uncompressed image and one compressed with reasonable parameters. The preprocessing time can however be increased depending on the algorithms chosen.

{{< figure src="pref.png" link="pref.png" caption="Prefences" >}}


## Ha and Ha/OIII extraction

These new functions allow the direct extraction of Halpha or Halpha / OIII layers from non-demosaiced CFA images. This is very useful when using a Ha filter, or a new Duo Narrowband with a color camera.

{{< figure src="Ha_1.png" link="Ha_1.png" caption="Pre-processed CFA image." >}}

{{< figure src="Ha_2.png" link="Ha_2.png" caption="Extracted Ha layer" >}}


## Linear Match function

Before compositing images taken with different filters (in the RGB composition tool for example), it is usually necessary to make them consistent with each other. Without it, the final image has a strong imbalance that is difficult to deal with. This is the goal of this new tool which takes a reference image as a parameter and transforms the loaded image to “match” the reference.


## Sky background extraction applied to a sequence

Background extraction is something that needs to be done almost every time. Whether it’s from light pollution, the moon, or any other light source, the stacked image has a gradient that must be removed. This gradient is the sum of the gradients of each sub. However, the mount moves, the intensity and the position of the lighting change over time, etc. In fact, the final gradient is a complex sum of different gradients and therefore it can be quite difficult to remove it. To fix this issue, Siril allows the gradient to be automatically removed on each pre-processed image (to be done before registration). This creates a new sequence and the stacked image will be much easier to process, although background extraction may still be necessary.


## Histogram transformation applied to the sequence

It is possible to apply the histogram transformation to the entire sequence. Only one button to do it.  

![mtf](mtf.png)


## New file format: Single FITS sequence file

The FITS format is a file format widely used in astrophotography. Developed by and for NASA, it is now well use by the amateur astrophotographer community.

What is less well known, however, is that it is possible to save multiple images into a single FITS file – such like a movie. Siril can now work with FITS files containing a sequence. To do this, simply select FITS sequence during the conversion, or during the pre-processing step. In scripts, this is the _-fitseq_ option.


## New deconvolution tool

The deconvolution tool has been completely rewritten. In addition to being now a “real-time” tool (with a preview), the algorithm is much more efficient and works with masks that are built automatically and transparently for the user. The stars are therefore much better protected and artifacts avoided.  

![deconv](deconv.png)

## New universal scripts

In versions of Siril prior to v0.99.4, scripts provided with the software were dedicated to DSLR only. Now scripts work regardless of the input image format (FITS or RAW). One also find scripts for extracting the Ha channel: at the end we end up with a monochrome image containing the signal Ha, or also a script for extracting Ha / OIII. This time we end up with two monochrome images Ha and OII.

## Trying to make FITS file more standard

Finally I would like to end with the effort that we have tried to make the FITS file more standard in the amateur astrophotographer community by introducing a new FITS keyword. This keyword was quickly adopted by a lot of software (even PixInsight that has dropped the FITS support), showing that there was a need for it. To learn more about this new feature, you can read this page: [https://free-astro.org/index.php?title=Siril:FITS_orientation](https://free-astro.org/index.php?title=Siril:FITS_orientation)

Of course, I introduced the main and funniest new features, but this version comes with a lot (a lot a lot …) of new features.

Hope you enjoy this thread, feel free to ask me about it.

Cheers

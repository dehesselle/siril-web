---
title: Get 15 % off our T-shirts and other accessories
author: Argonothe
date: 2019-03-12T15:50:04+00:00
featured_image: Capture-du-2019-03-16-18-57-19-1024x643.png
categories:
  - News
---

Help us promote your favorite free astronomy image processing software by wearing Siril's colors.

Discover the T-shirt and other accessories shop for the  <a href="https://shop.spreadshirt.com/sirils-shop" target="_blank" rel="noopener noreferrer">United states / Canada</a>, <a href="https://shop.spreadshirt.fr/siril-astro" target="_blank" rel="noopener noreferrer">other countries</a>.

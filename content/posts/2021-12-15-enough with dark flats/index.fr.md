---
title: Stop à la mode des dark flats
author: Cyril Richard
show_author: true
date: 2021-12-15T11:35:34+00:00
featured_image: comparo_flat_MDF.png
categories:
  - Nouvelles

---

De plus en plus, nous voyons fleurir sur les forums le terme `master dark flat`. Ceux-ci sont censés remplacer les `offsets` qui sont généralement utilisés dans le processus standard de prétraitement des images astronomiques. On lit que le résultat est meilleur en utilisant un dark de flat (enregistré avec le même temps de pose que le flat) qu'avec des offsets standards. Qu'en est il réellement ? D'où vient cette frénésie soudaine pour une image maître dont personne ne parlait il n'y a pas si longtemps ?

## A quoi sert l'offset ?
Tout d'abord, attardons nous sur l'utilité des darks, flats et offsets. 
- le `master dark` retire le signal thermique **ET** l'offset de l'image. **Attention**, il ne retire pas le bruit. En effet aucun des fichiers maitres ne retire de bruit car ce dernier, de nature aléatoire, va au contraire s'ajouter à l'image.
- le `master flat` aplatit l'image en supprimant le vignettage et les éventuelles poussières présentes sur le capteur
- L'utilité du `master offset` est plus difficile à saisir. Cependant, il est là pour enlever, comme son nom l'indique, un niveau d'offset qui a été ajouté par le constructeur pour éviter d'avoir des valeurs négatives lors du traitement de l'image par l'appareil photo et donc afin d'éviter de perdre de l'information. Les constructeurs d'appareil photo définissent une valeur qui est une puissance de 2 : `512`, `1024`, `2048` ... alors que pour les caméras astronomiques cela peut être n'importe quelle valeur.

La prise d'une image d'offset doit se faire à la vitesse la plus rapide possible, et ce afin d'éviter l'ajout de signal et bruit thermique qui viendrait parasiter le traitement. Cependant, un offset contiendra, en plus de son niveau d'offset, un certain niveau de bruit et ceci est inévitable. C'est d'ailleurs pour cela que l'on conseille de prendre le plus d'offsets possible afin de minimiser le bruit dans le master final. Rappelons en effet que lorsque l'on soustrait deux images, leurs bruits respectifs s'ajoutent. On introduit donc plus de bruit dans l'image finale en ayant utilisé 20 offsets, qu'en en ayant utilisé 200.

## Comparaison des méthodes
Maintenant pourquoi tant de personnes vantent les mérites du `dark de flat` ? 

Un `dark de flat` est censé contenir le signal thermique que l'on souhaite soustraire aux flats. Mais est-ce que les flats contiennent un signal thermique qui mérite d'être enlevé ? Rappelons que le signal thermique est un signal qui devient non négligeable sur une image lors de temps de pose suffisamment long. Il apparaît plus vite sur des capteurs non refroidis qui ont donc des températures plus élevées. Cependant, un flat est une image prise en pleine lumière, à des temps très courts généralement inférieur à 5s. Le fait que l'image soit prise en pleine lumière fait que son rapport signal sur bruit est très élevé par rapport aux autres images de la chaîne de traitement. Par conséquent, un éventuel signal thermique, qui plus est sur un temps de pose inférieur à 5s, est complètement négligeable et ne doit pas être pris en compte.

De plus, on s'appercoit sur la figure suivante que la seule différence notable entre un master offset et un master dark flat est la quantité de bruit contenu dans l'image maitre. En effet, le niveau est indépendant de l'exposition et seule la quantité de bruit augmente lorsque le temps de pose augmente.

{{<figure src="std_comparison.png" link="std_comparison.png" caption="Évolution du bruit des images masters (construites à partir de 100 images) en fonction du temps d'exposition. Alors que le bruit augmente avec le temps d'exposition, la valeur de la médiane reste constante.">}}

Il existe cependant une raison pour laquelle un `dark flat` peut s'avérer utile. En effet, avec l'utilisation de filtres à bande étroite, les temps de pose des flats peuvent devenir importants (dans certains cas plus de 10 secondes). De plus, certaines caméras possèdent un fort ampglow qui ne peut être corrigé autrement qu'avec un dark. Mais nous entrons ici dans un cas de figure peu courant, et néanmoins, nous vous encourageons à vérifier par vous même si l'utilisation de "dark flat" apporte quelque chose ou non.

## L'offset synthétique
Dans les dernières versions disponibles, Siril met à disposition la possibilité de [retirer directement un niveau](../../../tutorials/synthetic-biases/), sans passer par une image bruitée. L'avantage est énorme et allie la simplicité à l'efficacité : l'offset est bien retiré et aucun bruit n'est ajouté à l'image finale. De plus on économise du temps et de l'espace disque (donc du temps de traitement) en s'épargnant le fait de devoir faire une centaine d'offsets. 

Dans quel cas peut on utiliser l'offset synthétique ? Pratiquement dans tous les cas avec un capteur **moderne**[^1]. Pour s'en convaincre, il suffit d'ouvrir un offset dans Siril et d'analyser les statistiques afin de s'assurer qu'il est assez "plat". Attention, la visualisation en mode `égalisation d'histogramme` peut fortement exagérer les contrastes, passez alors votre souris sur l'image afin de vérifier la cohérence des valeurs des pixels. L'illustration suivante montre la distribution des pixels relative à la médiane de l'image. On voit très nettement que peu de pixels s'écartent de la valeur centrale.
{{<figure src="bias_stats.png" link="bias_stats.png" caption="La distribution des pixels d'un master offset montre que celle-ci est centrée sur la valeur de l'offset (ici 1920). Peu de valeurs se sitiuent en dehors.">}}

Pour comprendre pourquoi un `dark de flat` peut être remplacé par une constante, les figures ci-dessous comparent les valeurs de pixels en ADU, extraits d'un flat de 5s et du `master dark de flat` correspondant, pris avec une ZWO ASI294MM connue pour son ampglow (photos montrées avec l'aimable autorisation de @clouzot).
- Sur la ligne du haut, le master dark flat, en mode de visualisation Histogramme. Les 3 lignes de couleurs représentent 3 lignes le long desquelles les valeurs des pixels ont été extraites. Elles sont représentées sur la figure à droite de l'image.
- Sur la ligne du bas, la même chose mais pour un flat.
- La dernière figure à droite représente les mêmes valeurs, mais cette fois, tracées ensemble. A noter qu'une constante a été ajoutée aux valeurs du master dark flat, uniquement pour que toutes les courbes soient représentées sans dilater excessivement l'échelle.
Il apparait clairement que le master dark flat peut, sans souci, être remplacé par une ligne droite, c'est à dire un niveau constant. Que vous soustrayiez une constante ou le master ne fera pas de différence notable sur la courbure du flat.

{{<figure src="comparo_flat_MDF.png" link="comparo_flat_MDF.png" caption="Camparaison des valeurs le long du capteur, pour un flat de 5s et le master dark flat correspondant.">}}

## Conclusion
La conclusion de ce billet est simple : les `darks de flat` sont parfaitement inutiles la plupart du temps et un simple `master offset` suffit amplement. De plus, le retrait d'un offset synthétique, comme le permet Siril, peut s'avérer encore plus intéressant en évitant d'ajouter du bruit dans l'image.

[^1]: Il n'est pas possible d'utiliser un offset synthétique avec certains vieux capteurs CCD car les offsets de ces derniers peuvent contenir un gradient linéaire.

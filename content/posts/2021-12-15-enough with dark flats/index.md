---
title: Enough with dark flats
author: Cyril Richard
show_author: true
date: 2021-12-15T11:35:34+00:00
featured_image: comparo_flat_MDF.png
categories:
  - News

---
 
 
(...Or flat darks or whatever you call them)

More and more, we see the term `master dark flat` all over the forums. These are supposed to replace the `biases` that are usually used in the standard preprocessing of astronomical images. We read that the result is better using a dark flat (shot with the same exposure time as the flats) than with standard biases. But is this really the case? Why such a frenzy for a master image that no one was talking about not so long ago?

## What is the purpose of bias?
First of all, let us define what are the use of darks, flats and biases.
- `master dark` removes **both** the thermal signal and the bias of the image. **Attention**, it does not remove the noise. Indeed, none of the master files remove noise, because the latter, with its random nature, will instead be added to the image.
- the `master flat` flattens the image by removing the vignetting and the possible dusts present on the sensor. 
- The utility of the bias is more difficult to grasp. However, it is there to remove, as its name indicates, a level of offset that has been added by the manufacturer to avoid having negative values when processing the image by the camera and therefore to avoid losing information. DSLR manufacturers, most of the time, define a value that is a power of 2: `512`, `1024`, `2048` ... while for astronomical cameras, it can be any value.

Taking a bias image must be done at the fastest possible speed, to avoid adding signal and thermal noise that would interfere with the processing. However, a bias will contain, in addition to its offset level, a certain level of noise. This is why we advise you to take as many biases as possible in order to minimize the noise in the final master. Remember that when you subtract two images, their respective noise is added. Therefore, using 20 biases introduces more noise into the final image than using 200 biases.

## Comparison of methods
Now why do so many people praise the `dark flat`? 

A `dark flat` is supposed to contain as well the thermal signal that you want to subtract from the flats. But do the flats contain a thermal signal that is worth removing? Remember that the thermal signal is a signal that becomes non-negligible on an image when the exposure time is long enough. It appears more quickly on uncooled sensors which have higher temperatures. However, a flat is an image taken in full light, at times very short generally less than 5s. The fact that the image is taken in full light means that its signal-to-noise ratio is very high compared to other images in the processing chain. Therefore, a possible thermal signal, especially on an exposure time of less than 5s, is completely negligible and can just be disregarded.

Moreover, we can see on the following figure that the only notable difference between a master offset and a master dark flat is the amount of noise contained in the master image. Indeed, the level is independent of the exposure and only the noise increases when the exposure time increases.

{{<figure src="std_comparison.png" link="std_comparison.png" caption="Evolution of the noise of the master images (built from 100 frames) as a function of exposure time. While the noise increases with the exposure time, the median value remains constant.">}}

There may be a few cases where `dark flats` could be useful. Indeed, with the use of narrow band filters, the exposure time of flats may become important (in some cases more than 10 seconds). In addition, some cameras have a strong ampglow that can not be corrected otherwise than with a dark. But we enter here in an unusual case, and nonetheless, we encourage you to check whether using `dark flat` is a true necessity or not.

## Synthetic bias
In the latest versions available, Siril offers the possibility to [directly remove a level](../../../tutorials/synthetic-biases/), without using a noisy image. The advantage is huge and combines simplicity with efficiency: the bias level is removed from the flats and no noise is added to the final image. Moreover, one saves time and disk space (thus processing time) by avoiding having to shoot a hundred of bias images.

In which cases can synthetic biases be used? Provided you have a **modern** sensor, in most cases[^1]. To be convinced of this, just open a bias in Siril and analyze the statistics to make sure that the bias is "flat" enough. Beware that a Histogram preview may be exagerating the contrasts, so hover your mouse around the image to check the consistency of the values. We can clearly see that few pixels deviate from the central value.
{{<figure src="bias_stats.png" link="bias_stats.png" caption="The distribution of the pixels of a master bias shows that it is centered on the value of the offset (here 1920). Few values ​​lie outside.">}}

To understand why a `dark flat` can be safely replaced by a constant, the plots below compare pixels values in ADU for a flat frame of 5s and its matching master dark flat, both shot with a ZWO ASI294MM, known for having ampglow (pics shown courtesy of @clouzot). 
- On the top row, the master dark flat in histogram stretched view. The 3 colored lines materialize 3 rows along which pixel values have been extracted and are plotted on the figure directly to the right.
- On the bottom row, the same with a flat frame.
- The last plot to the right represents the same, but now the values extracted from the master dark flat and from the flat are plotted together. Note that a constant value has been added to the masterdark lines to avoid dilating the scale too much. 
And it now appears clearly that the masterflatdark can, without any doubt, be replaced by a straight line, i.e. a constant level. Whether you substract a constant or the actual master from the flat is not going to make a noticeable difference on the curvature of the flat.

{{<figure src="comparo_flat_MDF.png" link="comparo_flat_MDF.png" caption="Comparison of 5s flat and master dark flat pixel values along the width of the sensor.">}}

## Conclusion
The conclusion of this post is simple: the `flat darks` are, most of the time, useless and a simple `master bias` is more than enough. Moreover, removing a synthetic bias, as allowed by Siril, can be even more interesting by avoiding adding noise to the image.

[^1]: It is not possible to use a synthetic bias with some old CCD sensors because the bias of these sensors may contain a linear gradient.

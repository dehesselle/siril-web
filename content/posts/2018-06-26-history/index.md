---
title: History
author: Argonothe
date: 2018-06-26T13:22:35+00:00
categories:
  - News
---

<div class="post-content-inner">
  <ul>
    <li>
      <b>June 7, 2018</b> <ul>
        <li>
          The new <a title="Siril:0.9.9" href="http://free-astro.org/index.php?title=Siril:0.9.9">0.9.9</a> version has been released, with bug fixes, and new script feature.
        </li>
      </ul>
    </li>
  </ul>
  
  <ul>
    <li>
      <b>February 19, 2018</b> <ul>
        <li>
          A patch of the <a title="Siril:0.9.8" href="http://free-astro.org/index.php?title=Siril:0.9.8">0.9.8</a> version has been released: 0.9.8.3.
        </li>
      </ul>
    </li>
  </ul>
  
  <ul>
    <li>
      <b>January 31, 2018</b> <ul>
        <li>
          The new <a title="Siril:0.9.8" href="http://free-astro.org/index.php?title=Siril:0.9.8">0.9.8</a> version has been released, with bug fixes, improvements on lucky imaging and Windows integration.
        </li>
      </ul>
    </li>
  </ul>
  
  <ul>
    <li>
      <b>December 4, 2017</b> <ul>
        <li>
          A new 0.9.8 version is <a class="external text" href="https://free-astro.org/bugs/roadmap_page.php" rel="nofollow">progressing well</a> and will particularly help people working on deep-sky lucky imaging.
        </li>
        <li>
          We have also started working on features for the next major version of Siril, <b>version 1.0</b>. The first development, started at the beginning of the year, aims to greatly speed up the display of images and the rendering effects and this looks very promising. Other major developments will be the use of a better precision, floating point, internal image format that will give better results with some algorithms, the parallelisation of preprocessing and the creation of a high quality registration and stacking method for planetary images. It will still take a lot of time, but that will be a fully featured and fast version of Siril. Current 0.9.x versions are stable and already provide great results for deep-sky image processing.
        </li>
      </ul>
    </li>
  </ul>
  
  <ul>
    <li>
      September 21, 2017 <ul>
        <li>
          New release <a title="Siril:0.9.7" href="http://free-astro.org/index.php?title=Siril:0.9.7">0.9.7</a>. Click <a title="Siril:releases" href="http://free-astro.org/index.php?title=Siril:releases">here</a> for past releases. Siril is now available on <b>Windows</b> as a beta version!
        </li>
      </ul>
    </li>
  </ul>
  
  <ul>
    <li>
      June 20, 2017 <ul>
        <li>
          New release <a title="Siril:0.9.6" href="http://free-astro.org/index.php?title=Siril:0.9.6">0.9.6</a>. Click <a title="Siril:releases" href="http://free-astro.org/index.php?title=Siril:releases">here</a> for past releases.
        </li>
      </ul>
    </li>
  </ul>
  
  <ul>
    <li>
      November 28, 2016 <ul>
        <li>
          New release <a title="Siril:0.9.5" href="http://free-astro.org/index.php?title=Siril:0.9.5">0.9.5</a>. Click <a title="Siril:releases" href="http://free-astro.org/index.php?title=Siril:releases">here</a> for past releases. Development of major improvements such as native 32 bit images and GEGL rendering has started.
        </li>
      </ul>
    </li>
  </ul>
  
  <ul>
    <li>
      October 8, 2016 <ul>
        <li>
          A new team member has given a huge effort to bring you a fantastic new multi-language documentation, both <a class="external text" href="https://free-astro.org/siril_doc-en/" rel="nofollow">browsable</a> and in <a class="external text" href="https://free-astro.org/download/siril-doc-0.9.9-en.pdf" rel="nofollow">PDF</a>.
        </li>
      </ul>
    </li>
  </ul>
  
  <ul>
    <li>
      August 17, 2016 <ul>
        <li>
          New release <a title="Siril:0.9.4" href="http://free-astro.org/index.php?title=Siril:0.9.4">0.9.4</a>. Click <a title="Siril:releases" href="http://free-astro.org/index.php?title=Siril:releases">here</a> for past releases.
        </li>
      </ul>
    </li>
  </ul>
  
  <ul>
    <li>
      October 28, 2015 <ul>
        <li>
          First stable version available: <a title="Siril:0.9.0" href="http://free-astro.org/index.php?title=Siril:0.9.0">0.9.0</a>. Stability updates and minor improvements will occur in the dedicated 0.9 branch.
        </li>
      </ul>
    </li>
  </ul>
  
  <ul>
    <li>
      June 10, 2015 <ul>
        <li>
          New registration method available! It is now possible to register images with an automatic global star detection tool. The algorithm takes into account the translation and the rotation.
        </li>
      </ul>
    </li>
  </ul>
  
  <ul>
    <li>
      April 13, 2015 <ul>
        <li>
          We have been working hard on accelerating stacking algorithms on multi-core CPUs, giving SER a better support and we are also working on the two main lacks of Siril: taking into account rotation and multi-point in registration, for better deep-sky and planetary registrations. These works are in progress, and will take some weeks to complete.
        </li>
      </ul>
    </li>
  </ul>
</div>

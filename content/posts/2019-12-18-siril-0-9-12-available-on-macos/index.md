---
title: Siril 0.9.12 available on macOS
author: Cyril Richard
date: 2019-12-18T10:06:06+00:00
featured_image: /wp-content/uploads/2018/08/pkgimage.jpg
categories:
  - News
---

It is Christmas for all macOS users right now. Indeed, 0.9.12 version has been now released for this platform. You can then go to the [Download][1] section in order to get it right now.

 [1]: https://www.siril.org/download/

---
title: Siril 0.9.10
author: Argonothe
date: 2019-01-17T18:48:42+00:00
featured_image: /wp-content/uploads/2019/01/siril_logo-1024x1024.png
categories:
  - News
---

Siril 0.9.10 has been released and brings its load of new features and improvements. It provides a new comet registration method, astrometry solving, previewing for processing functions, and new commands available for scripts. Windows support has been improved, in particular for special characters in file names. We also have moved to a gitlab source and issue control system to facilitate interaction with users and developers. See other news in the release notes(<https://free-astro.org/index.php?title=Siril:0.9.10>).

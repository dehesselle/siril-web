---
title: We are looking for volunteer translators
author: Argonothe
date: 2018-07-14T16:48:23+00:00
featured_image: /wp-content/uploads/2018/07/The_world_flag_2006.png
categories:
  - News
---

In order to offer you the best, we are looking for volunteer translators.

## For the software:

No programming experience is required. See the [translation instructions][1] page for software translation.

In any case, check the status of your language translation with cyril (at free-astro.org) if you want to help.

## For the documentation and for the online courses:

Send an email to the following address : contact(at siril.org).

&nbsp;

Many thanks in advance for your assistance.

 [1]: https://free-astro.org/index.php?title=Siril:translate "Siril:translate"

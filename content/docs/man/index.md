---
title: "Siril Man Page"
author: Cyril Richard
---

## Name
siril - image processing tool for astronomy and others

## Synopsis
       Siril GUI
        siril <file>             Start Siril with file.

       Siril CLI
        siril-cli  [-i  conf_file]  [-f] [-v] [-h] [-p] [-d working_directory]
       [-s script_file] [image_file_to_open]

## Description
Siril is an image processing tool specially tailored for  noise  reduction  and  improving  the  signal/noise ratio of an image from multiple captures, as required in astronomy. Siril can align, stack and  enhance pictures  from  various  file formats, even image sequences (movies and SER files). It makes use of OpenMP, supports all the cameras  supported by  libraw, and carries out its calculations in a high precision 32-bit floating point engine.

## Options
       -i     Starts Siril with configuration file  which  path  is  given  in
              conf_file

       -f     (or  --format) Prints all supported image input formats, depend‐
              ing on the libraries detected at compile-time

       -v     (or --version) Prints program name and version and exits

       -h     (or --help) Short usage help

       -p     Starts Siril without the graphical user interface  and  use  the
              named  pipes to accept commands and print logs and status infor‐
              mation. On  POSIX  systems,  the  named  pipes  are  created  in
              /tmp/siril_commands.in and /tmp/siril_commands.out

       -d     (or --directory) Setting argument in cwd

       -s     (or  --script) Starts Siril without the graphical user interface
              and run a script instead. Scripts are text files that contain  a
              list  of  commands  to be executed sequentially. In these files,
              lines starting with a # are considered as comments.

       image_file_to_open
              Open an image or sequence file right after start-up

## Files
       ~/.config/siril/siril.cfg
              User preferences. Overridden by the -i option.
       ~/.siril/siril.css
              The stylesheet used to change colours of the graphical user  in‐
              terface. This is useful for personalisation or if a GTK theme is
              incompatible with some coloured elements of Siril.

## Links
* Website: https://www.siril.org/
* Documentation: https://free-astro.org/siril_doc-en/
* Forum: https://discuss.pixls.us/siril
* Code and bug reports: https://gitlab.com/free-astro/siril/issues
## Authors
* [Vincent Hourdin](debian-siril@free-astro.org)
* [Cyril Richard](cyril@free-astro.org)

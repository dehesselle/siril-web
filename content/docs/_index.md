---
title: "Docs"
author: Cyril Richard
menu: "main"
weight: 4
---

## Documentation
### Siril
* [Online Documentation](https://free-astro.org/siril_doc-en/)
* [PDF Documentation](https://free-astro.org/download/siril-doc-0.9.11-en.pdf) (old stable version 0.9.11)

### External tools
* Siril's compagnon: [Sirilic](sirilic)

## Commands
* [List of commands (Latest official version)](https://free-astro.org/index.php?title=Siril:Commands&oldid=8045)
* [List of commands (Development version)](https://free-astro.org/index.php?title=Siril:Commands)

## Siril Manual Pages
* [siril](man)

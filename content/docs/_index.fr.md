---
title: "Docs"
author: Cyril Richard
menu: "main"
weight: 4
---

## Documentation
### Siril (ancienne version stable 0.9.11)
* [Documentation en ligne](https://free-astro.org/siril_doc-fr/)
* [Documentation PDF](https://free-astro.org/download/siril-doc-0.9.11-fr.pdf)

### Outils externes
* Le compagnon de Siril : [Sirilic](sirilic)

## Commandes de Siril
* [Liste des commandes (version officielle)](https://free-astro.org/index.php?title=Siril:Commands)
* [Liste des commandes (version de développement)](https://free-astro.org/index.php?title=Siril:Commands)

## Pages du manuel Siril
* [siril](man)

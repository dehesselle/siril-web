---
title: "About"
author: Argonothe
date: 2020-09-06T10:13:55+00:00
menu: "main"
weight: 1
---

### Siril is an astronomical image processing tool.

It is specially tailored for noise reduction and improving the signal/noise ratio of an image from multiple captures, as required in astronomy.

Siril can align automatically or manually, stack and enhance pictures from various file formats, even image sequence files (films and SER files).

Contributors [are welcome](https://gitlab.com/free-astro/siril/-/blob/master/CONTRIBUTING.md).

Programming language is C, with parts in C++. Main development is done with most recent versions of shared libraries on GNU/Linux.

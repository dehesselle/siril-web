---
title: Photometry
author: Cyril Richard
show_author: true
featured_image: photometry_ref.png
rate: "3/5"
type: page
---

In this tutorial, we will explore the capabilities of Siril to perform photometric processing to display a light curve of a variable star. And in this example, the variability of the star comes from a transit of an exoplanet.

#### EPIC-211089792 b, an exoplanet in Taurus, discovered in 2016
##### Plate solving
Courtesy of **Gerard Arlic**, one of the co-discoverers of EPIC-211089792 b under the direction of the astrophysicist A. Santerne, I have in my hands a set of 111 images containing a transit of the planet, a hot Jupiter, in front of the star WASP-152. These images are excellent as an example and represent the best of the pro-am collaboration. The scientific paper of its discovery can be found [here](https://iopscience.iop.org/article/10.3847/0004-637X/824/1/55).
{{<figure src="wasp_sequence.png" caption="The WASP-152 sequence loaded in Siril" >}}
First of all, be sure to pre-process your images to remove the signal from darks and flats, otherwise your photometric data could be wrong.
Then, the first challenge is to find the variable star in the image. It can be tricky when we are not familiar with the star field, which is the case here. The `Image Plate Solver` tool of Siril is in this case a very good friend. For this sequence of images, I don't know the focal length nor the pixel size, which are input data for the tool. We will have to find them by guessing, trying various values until it works, for example by setting an arbitrary pixel size and modifying the focal length at each try. Quickly we got the result shown below. It was done on the first image of the sequence.
{{<figure src="plate_solving.png" caption="With trial and error, we can find parameters for which the plate solving suceeds. It could be useful to untick the `Flip Image if Needed` option when images are mirrored in order to preserve orientation.">}}
The console displayed the following astrometric parameters:

    20:17:50: Findstar: processing...
    20:17:50: Catalog PPMXL size: 843 objects
    20:17:50: 82 pair matches.
    20:17:50: Inliers:         0.585
    20:17:50: Resolution:      1.106 arcsec/px
    20:17:50: Rotation:     +179.92 deg 
    20:17:50: Focal:        1006.88 mm
    20:17:50: Pixel size:      5.40 µm
    20:17:50: Field of view:    01d 01m 56.91s x 46' 45.38"
    20:17:50: Image center: alpha:  04h10m13s, delta: +24°20'47"
    
Now that the plate solving succeeded, we have access to a lot of interesting features. Here we will use the object search, available in the context menu of the image (right click on it), the `Search Object...` entry (also available as `control+shift+/`). A small dialog box opens, in which we'll type `WASP-152` (or `EPIC-211089792`) and `Enter`.
{{<figure src="search_object.png" caption="We input again the name of the variable star." >}}
The star `WASP-152` becomes annotated, and this will help us greatly for the analysis.
{{<figure src="2022-01-10T16.17.59.png" caption="The variable star is now annotated." >}}

##### Photometry of the sequence
Now that we know where to find the star, we will start analysing it. First, the images have to be aligned, in case they show some movement between each other, to facilitate the analysis of the same star in all images. This will be very easy in our example since we have many stars visible on images, we can use the `Global registration` algorithm. We will activate the `Save Translation in seq file` option with `Translation`, for two reasons:
- When not checked, rotation is used and Siril will create a new sequence of rotated images, which doubles the space required on the disk. With translation only, the shifts between images are stored in Siril's sequence file.
- Not rotating the images also avoids unwanted interpolation between pixel values, so we keep the images as pristine as possible.

Note: obviously this trick will only work if your sequence contains no, or very little, field rotation.

{{<figure src="registration.png" caption="Aligning images is a key step for sequence photometry analysis." >}}
In my example, the registration executes quickly and without error.

    20:22:01: Sequence processing succeeded.
    20:22:01: Execution time: 36.52 s.
    20:22:01: Registration finished.
    20:22:01: 111 images processed.
    20:22:01: Total: 0 failed, 111 registered.
    
Before getting to the heart of the matter, with photometry, it is advisable to go to the `Graphics` tab in order to analyze the images and to deselect those likely to cause problems in the analysis. The latest versions of Siril have a formidable tool for this, with the possibility of displaying several selection criteria depending on the others. In particular, after a global alignment it is possible to display the `FWHM` with regards to the `roundness` of the stars. In the following illustration, I even chose to use the `wFWHM` which is an improved FWHM.
{{<figure src="dots_plot.png" link=="dots_plot.png" caption="The dot cloud represents the wFWHM as a function of the roundness of the stars. It is easy to identify the points (images) that cause problems." >}}
The image above shows us the result and we notice that 6 images can potentially be a problem. A click on the point allows to go to the image, and also offers the possibility to deselect it. In this tutorial I deselected all the suspicious points. This sorting can be refined in the next step.

Once all images aligned and sorted, we can actually analyse the photometry of the sequence. Let's start with the variable star, drawing a selection around it. Attention, if the selection is too small photometry will fail and this is why we recommend the middle mouse click. It draws a square centred on the pointer, large enough for photometry. Then, right click in the image, and select `PSF for the Sequence`.
{{<figure src="photometry_psf.png" caption="Photometry is done on the variable star for all images of the sequence." >}}
The photometry is run on the whole sequence for the selected star. It is now possible to display various plots in the Plot tab: star roundness, FWHM, Gaussian amplitude, magnitude, background level and X,Y positions in the images. The background level plot is of great interest. With it, we can see how images evolve during the acquisition session. To remove this bias from the measures, we will absolutely need to select more stars in the image, which will act as a reference against which the variable star's background can be calibrated.
Let's focus on another plot: magnitude. It is given in relative magnitude, only variations mean something. Here you can, in the same way as before, deselect images that you may find suspicious.
{{<figure src="magnitude.png" caption="Uncalibrated magnitude plot for the variable star." >}}
Now we want to run the photometric analysis for reference stars. Ideally, stars whose magnitude is stable across the sequence must be selected. If nearby stars are unknown, another feature is very useful:
- Select a star
- Context menu (right click) -> `PSF` (not for the sequence this time)
- Click on `More details...` in the new window
- This opens a Web browser page with data on the selected star.

Here, for learning purposes, I didn't push the analysis as far as finding stable stars in the image, and took 4 random stars. The variable star is variable enough for a probable good result any way. When choosing the reference stars, it is also a good idea to select stars whose brightness is alike the variable's, that are not too far from it, especially if flat field correction is not done (it should be mandatory for this kind of analysis), and not on the borders of images in case there is some shift between them. After having selected each reference star, run the `PSF for the Sequence` analysis.
{{<figure src="photometry_ref.png" caption="Four reference stars have been chosen in the image." >}}
Now when we display the magnitude plot, the button `Light curve` button becomes active, and clicking on it allows, as its name suggests, the light curve of the variable star to be plotted. A data file is saved, useful if the analysis is to be shared or for further analysis. If [gnuplot](http://www.gnuplot.info/) is installed, this will automatically display the light curve plot.
{{<figure src="wasp-152.png" caption="Light curve of `WASP-152`. We clearly see a dip indicating the transit of the exoplanet." >}}

We have just seen how, in a simple way, Siril allows to make a photometric study of an exoplanet transit. Obviously, this method is applicable to many other objects such as variable stars, occultation of stars by an asteroid, etc ... We encourage you to try the adventure and to share your results.

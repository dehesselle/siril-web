---
title: Photométrie
author: Cyril Richard
show_author: true
featured_image: photometry_ref.png
rate: "3/5"
type: page
---

Dans ce tutoriel, nous allons explorer les capacités de Siril pour effectuer des traitements photométriques afin d'afficher une courbe de lumière d'une étoile variable. Et dans cet exemple, la variabilité de l'étoile provient d'un transit d'une exoplanète.

#### EPIC-211089792 b, une exoplanète dans le Taureau, découverte en 2016
##### Résolution astrométrique
Grâce à l'aimable autorisation de **Gerard Arlic**, un des co-découvreurs de EPIC-211089792 b sous la direction de l'astrophysicien A. Santerne, j'ai entre les mains un jeu de 111 images contenant un transit de la planète, une Jupiter chaude, devant l'étoile WASP-152. Ces images sont excellentes pour servir d'exemple et représentent ce qui se fait de mieux en collaboration pro-am. L'article scientifique de sa découverte est consultable [ici](https://iopscience.iop.org/article/10.3847/0004-637X/824/1/55).
{{<figure src="wasp_sequence.fr.png" link="wasp_sequence.fr.png" caption="Séquence Wasp-152 chargée dans Siril" >}}
Avant toute chose, assurez-vous de pré-traiter vos images afin de bien retirer le signal de darks et de flats sans quoi vos données photométriques pourraient être erronées.
Ensuite, la première difficulté est de repérer l'étoile variable dans l'image. Chose aisée dans certains cas, cela peut s'avérer plus compliqué quand on ne connaît pas trop le champ photographié. L'outil `Résolution Astrométrique` de Siril s'avère dans ce cas là un outil précieux. Sur cette série d'images, je ne connais ni la focale utilisée, ni la taille des pixels de l'imageur. Il faut donc y aller par essais successifs en fixant une valeur de taille des pixels de façon totalement subjective, et faire varier la focale itérativement. Très rapidement, on arrive au résultat donné dans la figure ci-dessous. La résolution astrométrique a été faite sur la première image de la séquence.
{{<figure src="plate_solving.fr.png" link="plate_solving.fr.png" caption="En tâtonnant un peu, on arrive à trouver des paramètres satisfaisants pour réussir l'astrométrie. Il peut s'avérer indispensable de décocher l'option `Retourner l'image si nécessaire` lorsque les images sont en miroirs, afin de les garder orientées de la même façon." >}}
La console affiche alors les paramètres astrométriques suivant :

    20:17:50: Findstar : en cours...
    20:17:50: Taille du catalogue PPMXL : 848 objets
    20:17:50: 369 paires correspondantes.
    20:17:50: Pts OK :            0.846
    20:17:50: Résolution :        1.104 arcsec/px
    20:17:50: Rotation :       +179.92 deg 
    20:17:50: Focale :         1008.84 mm
    20:17:50: Pixel dim. :        5.40 µm
    20:17:50: Champ de vision :   01d 01m 51.88s x 46' 41.02"
    20:17:50: Centre de l'image : alpha :  04h10m13s, delta : +24°20'47"
    
La résolution astrométrique réussie, on a maintenant accès à tout un lot de fonctionnalités intéressantes. Ici, on va utiliser la recherche d'objet en cliquant droit sur l'image, puis sur `Chercher l'objet` (ou bien en utilisant le raccourci `ctrl + shift + /`). Une petite boite de dialogue avec une zone de texte s'ouvre. Il suffit de rentrer à nouveau le nom de l'étoile `WASP-152` (ou `EPIC-211089792`) et de valider avec la touche `Entrée`.
{{<figure src="search_object.png" link="search_object.png" caption="On rentre à nouveau le nom de l'étoile variable." >}}
L'étoile abritant l'éxoplanète `WASP-152` est alors annotée, et cela va grandement nous aider par la suite.
{{<figure src="2022-01-10T16.17.59.png" link="2022-01-10T16.17.59.png" caption="L'étoile variable est maintenant annotée." >}}

##### Photométrie de la séquence
Maintenant que nous avons repéré l'étoile au centre de notre attention, nous pouvons commencer l'analyse. Il faut en tout premier lieu aligner les images les unes avec les autres si elles ont des décalages entre elles, cela simplifiera l'analyse d'une même étoile sur toutes les images de la séquence. Rien de plus simple dans notre exemple car le nombre d'étoiles par image est largement suffisant. Le choix de l'alignement se porte donc sur `Alignement global`. Une particularité cependant est à noter ici : la case `Enregistrer la translation dans le fichier seq` est cochée avec l'option `translation`, et ce, pour plusieurs raisons :
- La première d'entre elles est que cette option permet d'économiser de la place sur le disque dur car aucune nouvelle séquence n'est créée : les informations d'alignement sont conservées dans le fichier `.seq`.
- Cela permet également d'éviter de créer des images où les pixels sont interpolés (lorsqu'il y a de la rotation), et les données brutes sont ainsi conservées. Si les images possèdent un petit décalage en rotation, cela n'est absolument pas problématique dans notre cas.

Note : évidemment cette astuce ne fonctionnera que si votre séquence ne contient pas, ou très peu, de rotation de champ.

{{<figure src="registration.fr.png" link="registration.fr.png" caption="L'alignement des images est une étape essentielle pour réussir la photométrie de la séquence." >}}
Dans mon exemple, l'alignement s'éxécute sans encombre en un temps très court.

    20:22:01: Le traitement de la séquence a réussi.
    20:22:01: Temps d'exécution: 26.03 s.
    20:22:01: Alignement fini.
    20:22:01: 105 images traitées
    20:22:01: Total : 0 en échec, 105 alignées.
    
Avant de rentrer dans le vif du sujet, avec la photométrie, il est conseillé d'aller dans l'onglet `Graphique` afin d'analyser les images et de désélectionner celles susceptibles de poser problème dans l'analyse. Les dernières versions de Siril possèdent un outil redoutable pour ça, avec la possibilité d'afficher plusieurs critères de sélection en fonction des autres. Notamment, après un alignement global il est possible d'afficher la `FWHM` en fonction de la `rondeur` des étoiles. Dans l'illustration suivante, j'ai même choisi d'utiliser la `wFWHM` qui est une FWHM améliorée.
{{<figure src="dots_plot.png" link=="dots_plot.png" caption="Le nuage de point représente la wFWHM en fonction de la rondeur des étoiles. On identifie facilement les points (images) qui posent probleme." >}}
L'image ci-dessus nous présente le résultat et l'on remarque que 6 images peuvent potentiellement poser problème. Un clic sur le point permet d'aller à l'image, et offre également la possibilité de la désélectionner. Dans le cadre de ce tuto j'ai désélectionné tous les points suspicieux. Ce tri peut encore être affiné à l'étape suivante.

Il est maintenant temps de passer à la partie photométrique et pour ce faire, nous allons commencer sur l'étoile dont la luminosité est variable, en traçant une sélection tout autour. Attention, si la sélection est trop petite, la photométrie échouera et c'est pourquoi nous recommandons le clic du milieu de la souris. Il dessine un carré centré sur le pointeur et suffisamment grand pour la photométrie. Puis, à l'aide d'un clic droit pour faire apparaître le menu, on choisit `PSF de la séquence`.
{{<figure src="photometry_psf.png" link="photometry_psf.png" caption="La photométrie est faite sur l'étoile variable pour toutes les images de la séquence." >}}
La photométrie est alors exécutée sur toute la séquence. Il est maintenant possible d'afficher de nouvelles courbes dans l'onglet `Graphique` : rondeur, FWHM, amplitude, magnitude, fond de ciel et les positions en X et Y.
Une autre courbe nous intéresse ici tout particulièrement : la magnitude. Elle est donnée en valeur relative, et seules les variations comptent, pas les valeurs absolues. Vous pouvez ici, de la même manière que précédemment, supprimer des images qui vous paraitrait suspectes. Une fois fait, voici à quoi ressemble l'affichage :
{{<figure src="magnitude.png" link="magnitude.png" caption="Courbe de la magnitude de l'étoile telle qu'on peut l'afficher dans l'onglet `Graphique`." >}}
On cherche maintenant à effectuer la photométrie sur les étoiles de référence. Idéalement il faut choisir des étoiles qui ne sont pas variables, ou au moins avec une variabilité négligeable par rapport à l'étoile analysée. Si on ne connaît pas les étoiles en périphérie de `WASP-152`, une option peut s'avérer utile :
- On sélectionne une étoile
- Clic droit -> `PSF` (pas sur la séquence cette fois)
- Clic sur `Plus de détails` sur la nouvelle fenêtre ouverte.
- Une fenêtre internet s'ouvre avec les données sur l'étoile sélectionnée.

Ici, dans un but didactique, je n'ai volontairement pas poussé l'analyse autant en profondeur en choisissant au hasard 4 étoiles de référence. On essayera cependant de prendre des étoiles à la luminosité assez semblable, et situées pas trop près du bord de l'image (Attention à ce que les étoiles soient présentes sur toutes les images de la séquence !!). Après sélection de chaque étoile, relancer l'analyse `PSF de la séquence`.
{{<figure src="photometry_ref.png" link="photometry_ref.png" caption="Quatre étoiles de référence ont été choisies dans l'image." >}}
Lorsque l'on affiche les courbes de magnitude, le bouton `Courbe de Luminosité` se dégrise et permet d'afficher, comme son nom l'indique, la courbe de lumière de l'étoile variable. Un fichier de données est également enregistré à des fins d'analyse plus poussées. Attention cependant, l'affichage d'une telle courbe nécessite l'installation au préalable du logiciel [gnuplot](http://www.gnuplot.info/).
{{<figure src="wasp-152.png" link="wasp-152.png" caption="Courbe de luminosité de l'étoile. On voit très nettement un creux indiquant le transit de l'exoplanète." >}}

Nous venons de voir comment, de façon simple, Siril permet de faire une étude photométrique d'un transit d'exoplanète. Évidemment, cette méthode est applicable à bien d'autres objets telles que les étoiles variables, occultation d'étoiles par un astéroïde, etc ... Nous vous encourageons à tenter l'aventure et à partager vos résultats.

---
title: Image Analysis
author: Cyril Richard
show_author: true
featured_image: global_psf.png
rate: "2/5"
type: page
---

In this tutorial, we will analyze an image thanks to its stars. What does FWHM mean? Why is it a good measure of the quality of your picture? 

#### Dynamic PSF tool
Dynamic PSF has been present for quite a while in Siril. It can very be useful in many occasions, like global star registration. But a vast majority of users do not know how to use it.
{{< figure src="dpsf-tool-en.png" caption="Dynamic PSF window when star detection has been activated." >}}
It is a powerful and easy-to-use tool, which will tell a lot about the quality of an image. To open it, head to the "Burger" menu, `Image Information` and `Dynamic PSF`.

##### A bit of theory
**Point Spread Function**, a.k.a. [PSF](https://en.wikipedia.org/wiki/Point_spread_function), is a mathematical function that describes the response of an imaging system to a point source. In theory, stars being far far away from us, they could be considered as perfect point sources. Well, in theory. In practice, their photons will interact with different media before reaching us: the Earth atmosphere, the glass of our refractors (or the mirror of our reflectors) and the camera sensors. All these interactions will tend to spread the initial source point, hence the name of the function. To keep it simple, if you are imaging with a high-end optics under a Chile-like atmosphere, the stars will look pin-point sharp. On the contrary, imaging with a toyscope just above the horizon will end up with large fuzzy stars.

###### Modelling the PSF
In Siril, the PSF is approximated by an elliptic gaussian. This function was selected thanks to its shape, particularly well suited to represent star-like objects. Once the fitting is done, it returns important parameters like FWHM, roundness, magnitude, etc...
- Ch: Channel where the PSF was applied.
- B: Average of local background.
- A: Amplitude of the fitted PSF (maximum value).
- (x<sub>0</sub>, y<sub>0</sub>): Centroid coordinates in pixel units.
- (FWHM<sub>x</sub>, FWHM<sub>y</sub>): The FWHM on the X and Y axis in PSF coordinates.
- Mag: Relative magnitude of the star.
- r: Roundness of the star, r=1 means a perfect round star.
- Angle: Rotation angle of the X axis of PSF coordinates in degrees.
- RMSE: Root-Mean-Square Error of the fit. 

###### FWHM as a selection criterion
The figure below shows a 1-D gaussian curve. Its FWHM (*Full Width at Half Maximum*) represents the width of the curve when the value is at half its maximum.
{{< figure src="fwhm.png" caption="Full Width at Half Maximum (FWHM)." >}}
To represent a star, we use a 2-D gaussian and FWHM is measured along the two main axes, x and y, either in pixels, or in arcseconds (if image sampling is known). FWHM can be seen as a measure of the star spreading wrt. a single point.

###### Roundness as another criterion
The roundness of a star is defined as the ratio between its FWHM along y-axis and x-axis. A roundness of 1 describes a perfectly round-shaped star while a roundness of 0.5 defines a star twice as large along x-axis than along y-axis. It is therefore an additional measure which, associated to FWHM (filters can be applied on both values at stacking stage), enables to narrow down the selection of your sharpest images.

{{< figure src="Star_roundness_en.png" >}}

##### Using Dynamic PSF tool
There are two ways to use this tool in Siril.
1. One star at a time, selecting them with care. Select an area around a star then right click and select `Pick a star`. This will add the star to the Dynamic PSF tool. The main advantage is that each star added to the tool has been chosen by the user.
{{< figure src="pick_a_star.png" caption=" Manual selection can provide better results as the number and which stars are selected is controlled by the user." >}}
1. Globally, by clicking on the star detection button. Siril will automatically detect anything that looks like a star. Frequently, the algorithm will detect bright objects as false positive, as can be seen in the figure below. While this has not impact of the registration process, the result of the analysis will be of lesser quality. Adjusting the detection threshold (increasing it in that case) will give better results.
{{< figure src="global_psf.png" caption="Global star detection as done by the star registration algorithm. For the sake of keeping the calculations quick, the gaussian function fitting does not account for the angle of the function, contrarily to when it is done one star at a time. This has very little impact on the final result." >}}

Once all the stars have been loaded in the window, you can sort them by any parameter, in increasing or decreasing order. You can also export this list in `csv` format by clicking on the appropriate button in the toolbar.

Finally, and it is one of the main merits of the tool, you can get average values of the parameters for all the stars selected in the image.
{{< figure src="psf-summary-en.png" >}}

###### Use case
When global registration fails, this tool can be very helpful to understand why and fix star detection. Head to the Dynamic PSF window and play with the detection parameters like threshold and roundness. After each change, don't forget to check the effect by clicking again on the detection button.
{{< figure src="star_detect.png" >}}
Once you have found a combination of parameters suitable for your image, perform again a global registration. It will use this updated set of detection parameters.

Here is a particular example of an image where the default parameters do not allow a global registration: only one star is detected. However, it is easy to see in autostretch mode that several stars are present in the image, and even in sufficient number to allow registration.
{{<figure src="global_issue1-en.png" link="global_issue1-en.png" >}}
The first modification that we are tempted to try is to lower the threshold level, in order to find stars at the noise limit. However, we see in our example that the improvement is very low: 3 stars detected, while only 1 previously. We could then think that it is necessary to lower the level of the threshold even further, but if we look closely at the stars detected, we realize that these are weak and small in size.
{{<figure src="global_issue2-en.png" link="global_issue2-en.png" >}}
The parameter that must be changed here is the radius of the detection box. The latter is indeed too small to detect the stars in the image. The image below proves it, with a radius whose size has been doubled.
{{<figure src="global_issue3-en.png" link="global_issue3-en.png" >}}

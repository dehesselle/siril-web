---
title: Traitement d'images complet (pré-traitées avec script)
author: Colmic
show_author: true
featured_image: result-crop.fr.png
rate: "1/5"
type: page
---

Ce tutoriel, effectué sous `Microsoft Windows`, est divisé en 3 parties. Il est utile à tout possesseur d'Appareil Photo Numérique (APN) ou de caméra Astro puisqu'une des améliorations de Siril 0.99 est l'arrivée des scripts universels.

Ces scripts sont dorénavant valables sans distinction pour APN (fichiers `RAW`) et pour caméra Astro (fichiers `FITS`).

Ce tutoriel va vous permettre de :

* Pré-traiter vos images brutes (mono ou couleur) avec retrait des darks, flats et offsets sous Siril de façon automatique à l'aide des scripts, dans plusieurs situations possibles
* Réaliser un premier traitement sous Siril (étalonnage des couleurs, ajustement de l'histogramme, retrait du gradient, etc..) et de sauvegarder l'image en `FIT`, `TIFF`, `JPG` ou `PNG` pour traitement ultérieur

Le tutoriel décrit ci-dessous est issue d'une série de tutoriels publiés sur le site [webastro](https://www.webastro.net/forums/topic/166799-tutoriel-pour-le-traitement-complet-dune-image-avec-les-scripts-siril).

Les points abordés dans la première partie (introduction) : [ci-dessous](#tuto-1).

* Les pré-requis pour bien démarrer Siril.
* Passer Windows en mode développeur.
* Comment bien réaliser ses brutes, darks, offsets et flats.

Les points abordés dans la seconde partie (script couleur) : c'est [ici](#tuto-2).

* Déposer ses images dans les bons dossiers avant le pré-traitement.
* Nettoyer le dossier Process avant de lancer un script.
* Lancer un script de traitement couleur basique.
* Ouvrir l'image Résultat.
* Modifier le mode d'affichage.

Les points abordés dans la troisième partie (traitement couleur) : [c'est là](#tuto-3).

* Recadrer l'image.
* Extraire le gradient.
* Étalonner les couleurs par photométrie.
* Réaliser une déconvolution.
* Transformer l'image en linéaire avec asinh.
* Ajuster l'histogramme.
* Supprimer le bruit vert.
* Augmenter la saturation.
* Sauvegarder l'image en `FITS`, `TIFF`, `JPG` ou `PNG`.

Logiciels nécessaires :

* La dernière version de Siril à télécharger [ici](https://siril.org/download/).
* Les nouveaux scripts pour Siril 0.99, soit disponibles par défaut dans l'installation, soit téléchargeables [ici](https://free-astro.org/index.php?title=Siril:scripts/fr#Obtenir_des_scripts).

### IMPORTANT AVANT DE COMMENCER :<a name="tuto-1"></a>

* Si vous aviez une ancienne version de Siril, il vous faut impérativement la désinstaller complètement (y-compris tous les scripts que vous auriez pu télécharger) avant d'installer la nouvelle.
* Si vous ne voyez pas de menu Scripts dans Siril ou si ne vous ne voyez pas les nouveaux scripts, effectuez la procédure décrite [ici](https://free-astro.org/index.php?title=Siril:scripts/fr#Utiliser_les_scripts).
* Si vous avez changé le dossier de travail de Siril (par défaut après installation dans \IMAGES ou \MES IMAGES selon votre version de Windows, ce qui correspond en réalité au dossier C:\USERS\VOTRE_NOM\PICTURES), vérifiez bien que les 4 sous-dossiers `brutes`, `darks`, `flats` et `offsets` se trouvent dedans.
* La version 0.99.x utilise les liens symboliques bien connus de Linux, ce qui permet d'économiser pas mal de Go sur votre disque lors du traitement, mais implique de passer votre Windows en mode développeur :
    * Cliquez sur le Menu Windows (ou la touche Windows du clavier) puis sur `Paramètres` (la roue crantée)
    {{<figure src="windows-dev.png">}}
    * Cliquez sur `Mise à jour et sécurité`
    {{<figure src="windows-update.fr.png">}}
    * Cliquez sur `Espace développeurs`, puis cochez `Mode développeur`
    {{<figure src="windows-dev-enable.fr.png">}}
    * C'est terminé, vous pouvez laisser comme ça éternellement, le mode développeur n'impacte rien sur le fonctionnement normal de Windows
    
#### Allez, vous êtes prêts ?

Attendez, tout d'abord, parlons de notre séance de prise de vue. Avant de continuer à lire ce tuto, vous devriez avoir obtenu :
* Un certain nombre d'images `brutes`, soit avec un APN (fichiers RAW genre ARW sur Sony, CR2 sur Canon, etc...), soit avec une caméra Astro mono ou couleur (fichiers `FITS`)
* Entre 10 et 100 images de DARK (toujours des `RAW` ou des `FITS`, prises dans le noir absolu, au même temps de pose, ISO ou GAIN que les `brutes`)
* Entre 10 et 20 images de `flat` (toujours des `RAW` ou des `FITS`), cette fois prises sous une lumière uniforme, par exemple à l'aide d'un écran à flat, ou de jour sur le fond de ciel tamisé par un T-shirt blanc, etc..., à l'ISO ou le GAIN le plus faible possible idéalement)
* Ajuster le temps de pose du `flat` de sorte que l'histogramme de l'image se situe environ aux 2/3 du maximum
* Entre 10 et 100 images d'`offset` (toujours des `RAW` ou des `FITS`, prises dans le noir absolu au temps d'exposition le plus court possible)
    
Dans les tutos ci-dessous j'ai utilisé :

* Une lunette de 106mm de diamètre et 388mm de focale (F/3.7)
* Une monture équatoriale motorisée sur les 2 axes
* Un diviseur optique équipé d'une caméra de guidage (ASI290 mini)
* Une caméra `ZWO ASI2600MC` réglée au GAIN 100
* Un filtre anti-pollution Optolong L-Pro (car oui même dans la Drôme la pollution lumineuse fait son œuvre, surtout à l'horizon sud où se trouvaient M8 et M20)
* Le tout piloté par un boîtier `ZWO ASiair pro` {{<figure src="FSQ_Chavadrome2.jpeg">}}

et obtenu :

* 15 images `brutes` de 180s à gain=100 des nébuleuses M8 et M20
* 15 `darks` de 180s à gain=100
* 15 `flats` de 3s à gain=100
* 15 `offsets` de 3s à gain=100 (dans ce cas de pose assez longue, on appelle plutôt ça des `darks` de `flats`)

Voici pour information une image brute de 180s, comme ça vous voyez de quoi on part... et à quoi on arrivera 
{{<figure src="bruteM8M20.jpeg">}}

### Alors on commence... par déposer ses images au bon endroit...<a name="tuto-2"></a>

Pour vous aider à vous faire la main, je vous ai mis en partage les `ZIP` de toutes les images `brutes`, `darks`, `flats` et `offsets` utilisées dans ce traitement.

Vous pouvez les récupérer [ici](http://www.astrosurf.com/colmic/Traitement_SiriL/brutes/).

* Copiez vos images dans les dossiers correspondants (j'ai bien dit copiez, pas déplacez, gardez toujours une copie de sauvegarde de vos brutes, on ne sait jamais) :
    * Copiez vos images brutes dans le sous-dossier brutes du répertoire de travail de Siril (donc par défaut dans `images/brutes`)
    * Copiez vos darks dans le sous-dossier darks du répertoire de travail de Siril (donc par défaut dans `images/darks`)
    * Copiez vos offsets dans le sous-dossier offsets du répertoire de travail de Siril (donc par défaut dans `images/offsets`)
    * Copiez vos flats dans le sous-dossier flats du répertoire de travail de Siril (donc par défaut dans `images/flats`)
    {{<figure src="selection-images.fr.png">}}
* Une des améliorations de Siril 0.99 est l'arrivée d'un nouveau dossier (se trouvant dans votre dossier de travail) appelé `Process`. C'est dans ce dossier que tous les fichiers de pré-traitement intermédiaires seront stockés. Vous pouvez supprimer ce dossier une fois tous vos traitements terminés afin de gagner de la place, personnellement je le fais systématiquement avant tout traitement sur un nouvel objet. {{<figure src="process.fr.png">}}

#### Puis on va lancer le script de pré-traitement des images avec Siril (calibration, alignement, empilement)...

* Démarrez Siril (normalement une icône Siril s'est installée sur le bureau pour les utilisateurs de Windows)
* Vérifiez toujours avant de lancer un script que le répertoire de travail par défaut est le bon, car il se peut, en cas d'erreur d'un script par exemple, que ce répertoire change.
    * Pour cela, cliquez sur l'icône `Maison` {{<figure src="siril-cwd-home.fr.png">}}
    * Puis sélectionnez votre dossier de travail qui doit contenir les sous-dossiers `brutes`, `darks`, `flats` et `offsets` (par défaut `Pictures`) et cliquez sur `Ouvrir` {{<figure src="siril-cwd.fr.png">}}
* Cliquez sur le menu `Scripts` puis sur le script Couleur_Pre-traitement {{<figure src="siril-scripts-list.fr.png">}} {{<figure src="script-running.fr.png" caption="Le script est lancé">}}

La durée du traitement va dépendre de :

* la puissance de votre ordi, de la taille de sa RAM, du type de disque dur (SSD fortement recommandé), etc..,
* mais aussi de la taille des fichiers de vos images brutes (traiter des images de 12Mpixels c'est bien plus rapide que de 62Mpixels !),
* bien évidemment du nombre d'images à empiler,
* et encore de l'option drizzle ou pas (compter 2x plus de temps avec drizzle).

Pour mon exemple précis :

* sur un PC portable `Gamer Asus ROG de 2015`, `i7 2.5Ghz`, `32Go de RAM`, `CG GTX980` et `SSD Samsung 1To`,
* pour un script de pré-traitement couleur basique (sans Drizzle),
* pour pré-traiter, aligner et empiler les 15 `brutes` avec 15 `darks`, 15 `flats` et 15 `offsets` (26Mpixels par image),

il aura fallu très exactement 2 minutes et 03 secondes au total pour que le script se termine. {{<figure src="script-end.fr.png">}}

#### On va maintenant récupérer l'image résultante de l'empilement...

* Cliquez sur le menu `Ouvrir` {{<figure src="open-button.fr.png">}}
* Double-cliquez sur le fichier `Resultat.fit` {{<figure src="open-result.fr.png">}}
* L'image devrait s'afficher dans la fenêtre de visualisation, tout d'abord en noir et blanc (par défaut sur la couche Rouge).
* Si l'image est toute noire, c'est tout à fait normal car le mode d'affichage est en linéaire :)
* En bas de l'écran, là où vous lisez `Linéaire`, sélectionnez `Auto-ajustement` {{<figure src="result-linear.fr.png">}}
* C'est mieux là non ? Vous pouvez maintenant visualiser l'image en couleur en cliquant sur `RVB` {{<figure src="result-autostretch.fr.png">}}
* Si l'image est toute verte comme ci-dessous, ne vous inquiétez pas, c'est normal ! {{<figure src="result-autostretch-rgb.fr.png">}}

#### On commence par redécouper l'image...<a name="tuto-3"></a>

* Ceci est très important pour la suite, car si l'on conserve les bords noirs, certains traitements ne se feront pas correctement sous Siril. Cependant, depuis la version 0.99.10 l'image empilée ne devrait plus avoir de bandes sombres.
* Tracez une zone dans une des couches `Rouge`, `Verte` ou `Bleue` (ne fonctionne pas sur l'image RVB) à l'aide de la souris afin de virer les bords noirs.
* Effectuez un clic-droit dans la zone tracée puis sélectionnez `Recadrer` : {{<figure src="result-crop.fr.png">}}

#### On va supprimer le gradient de l'image.

* Bien que cette image ait été réalisée dans la Drôme sous un excellent ciel, M8 et M20 étaient très bas sur l'horizon sud, dans la pollution lumineuse de la Côte d'Azur.
    * A noter qu'avec cette version 0.99 on peut maintenant supprimer le gradient avant l'empilement des images (directement sur la séquence d'images calibrées et dématricées), ce qui donne de meilleurs résultats qu'après.
    * Mais cela fera l'objet d'un autre tuto plus tard, ici on se contente de choses simples
* Toujours à partir d'une couche R, V ou B, cliquez sur menu `Traitement de l'image` puis sur `Extraction du gradient...`
{{<figure src="menu-bkg-extract.fr.png">}}
* Cliquez sur `Générer` {{<figure src="bkg-extract-generate.fr.png">}}
* Siril va alors générer des petits carrés un peu partout dans l'image, qui serviront à calculer l'extraction du gradient.
* Vous allez devoir maintenant cliquer-droit sur certains carrés (sur une des couches R, V ou B, ne fonctionne pas sur l'image RVB) se trouvant sur les vraies nébulosités pour les retirer du calcul :
{{<figure src="bkg-extract-sample.fr.png">}}
* Cliquez maintenant sur `Appliquer` :
{{<figure src="bkg-extract-apply.fr.png">}}
* Ce qui nous donne après l'extraction du gradient :
{{<figure src="bkg-extract-result.jpeg">}}
* notre cas de M8 et M20, avec beaucoup de nébulosités un peu partout dans l'image, sous un bon ciel et avec un filtre L-Pro, l'extraction du gradient est peut-être contre-productive.
* Vous avez néanmoins les billes pour l'appliquer ou pas selon les cas rencontrés.
* N'oubliez pas que vous pouvez toujours revenir en arrière dans le traitement en cliquant sur l'icône `annuler`

#### On va maintenant ajuster le fond de ciel et la balance des couleurs...

* Siril 0.9.11 avait apporté une grosse amélioration à ce niveau : l'étalonnage des couleur par photométrie
* Pour cela, Siril va réaliser une astrométrie sur l'image et comparer la couleur des étoiles de l'image avec celles de la base de données photométrique, et ajuster le tout pour refléter la réalité.
* Cliquez sur `Traitement de l'image`, puis sur `Étalonnage des couleurs`, puis sur `Étalonnage des couleurs par photométrie...` :
{{<figure src="menu-pcc.fr.png">}}
* Dans la zone de recherche, tapez le nom de l'objet photographié (ici `M8`) puis cliquez sur `Rechercher` :
    * Attention : vous devez être connecté à Internet pour effectuer cette opération.
    * Siril va alors interroger les bases de données astro et afficher le résultat comme ci-dessous :
{{<figure src="pcc-search.fr.png">}}
* Cliquez sur l'objet trouvé dans la base `Simbad` (ou `Vizier`), ici `Lagoon Nebula`.
* Vous pouvez cliquer sur `Obtenir les Métadonnées de l'image` pour récupérer automatiquement la focale et la taille des pixels de l'APN ou de la caméra (si ces données sont disponibles) :
{{<figure src="pcc-apply.fr.png">}}
* Si les Métadonnées n'existent pas, entrez alors manuellement la valeur de la focale
    * Attention, pour les versions inférieures à la 0.99.10, nous conseillions de doubler la focale pour les images faites avec Drizzle. Ceci n'est plus du tout vrai car la taille des pixels a été adaptée. Cependant, il est parfois difficile de faire la résolution astrométrique sur une image empilée avec Drizzle. Dans ce cas, une nouvelle option a été ajoutée à partir de la version 0.99.10 : "Sous-échantilloner l'image". Cliquer dessus sans rien changer aux valeurs, l'astrométrie devrait réussir et plus rapidement. Cocher cette option ne modifie pas votre image, uniquement une copie utilisée par l'algorithme d'astrométrie.
    * Et entrez la taille des pixels de votre APN (on peut le trouver [ici](https://www.digicamdb.com/)) ou de la caméra (dispo sur le site du constructeur).
* Remarquez ici que la case `Retourner l'image si nécessaire` est cochée. Cela permet de mettre l'image dans le bon sens si celle-ci ne l'est pas. Je vous conseille de la laisser cochée.
* Cliquez enfin sur `Valider`.
* Siril va alors faire un calcul astrométrique (qui peut prendre quelques minutes) afin de repérer les étoiles présentes dans l'image puis un calcul photométrique à partir de ces étoiles pour ajuster les couleurs.
{{<figure src="pcc-result.fr.png">}}
    * Si l'astrométrie sur l'image retourne une erreur, essayez de modifier la focale.
    * Si après plusieurs tentatives avec des focales différentes cela ne donne rien, essayez de rechercher un autre objet présent dans l'image (ou une étoile par son nom ou son numéro HD, HIP, etc..).
* Ce qui nous donne après l'ajustement des couleurs par photométrie : {{<figure src="pcc-image-finale.jpeg">}}

#### Une petite déconvolution pour commencer...

La déconvolution va améliorer la forme de vos étoiles, améliorer le "piqué" de l'image en général et les détails dans les nébulosités. Bien que certains préfèrent appliquer cet outil une fois l'histogramme de l'image étirée, il est préférable de le faire à cette étape : cela évite une montée trop importante du bruit.

* Cliquez sur le menu `Traitement de l'image`, puis sur `Déconvolution...` : {{<figure src="menu-deconv.fr.png">}}
* Dans la fenêtre de déconvolution, réglez le rayon et le booster en observant méticuleusement l'image au zoom 100% (`ctrl` + `molette souris`), ou bien en cliquant sur le bouton `1` dans la barre d'outil : {{<figure src="deconv-dialog.fr.png">}}
* Attention à ne pas être trop gourmand car du bruit et des artefacts peuvent apparaître rapidement comme ici : {{<figure src="deconv-dialog-2.fr.png">}}
* Cliquez sur `Appliquer` pour finaliser l'opération lorsque l'allure des étoiles et de l'image vous plaît. 

#### On va réaliser un petit asinh sur l'image...

Cette fonction est très utile avant de réaliser l'ajustement de l'histogramme, pour éviter de modifier les couleurs de l'image et de cramer des zones proches de la saturation comme un noyau de galaxie par exemple.
* Pour cela il va nous falloir repasser l'image en mode linéaire.
* Normalement vous devriez savoir le faire. Pour rappel, cliquez sur le menu déroulant où vous lisez `Auto-ajustement` et sélectionnez `Linéaire` : {{<figure src="menu-linear.fr.png">}}
* Cliquez sur `Traitement de l'image` puis sur `Transformation asinh...`  {{<figure src="menu-asinh.fr.png">}}
* Ajustez le facteur d'étirement et le point noir en surveillant l'image, de sorte qu'elle apparaisse doucement sans être trop claire.
{{<figure src="asinh-dialog.fr.png">}}
* Ce qui nous donne avec les valeurs ci-dessus :
{{<figure src="asinh-image.jpeg">}}

#### On va ajuster l'histogramme...

Il est important de vérifier que vous êtres bien mode d'affichage Linéaire pour passer cette étape, comme vu à l'étape précédente.
* Cliquez sur l'icône `Histogramme` : {{<figure src="histogram-icon.fr.png">}}
* Important : vérifiez toujours que le curseur du haut se trouve bien au maximum (valeur 65535) {{<figure src="sliders-max.fr.png">}}
* Dans la fenêtre Histogrammes, cliquez sur le `+` pour augmenter le zoom du graphe et cliquez sur l'engrenage : {{<figure src="histogram-dialog.fr.png">}}
* Vérifiez ensuite que la valeur de perte ne dépasse pas 0.1% en bas à droite, si besoin ajustez le curseur des basses lumières : {{<figure src="histogram-gear.fr.png">}}
* Si la perte est beaucoup plus élevée que 0.1%, vérifiez que vous avez bien recadré l'image et qu'il ne subsiste plus de bord noir, c'est généralement la principale cause.
* Maintenant vous pouvez jouer sur le curseur `Tons moyens` (celui du milieu) pour ajuster au mieux votre image.
* Ne jamais toucher au curseur Hautes lumières (qui doit se trouver complètement à droite du graphe).
* Cliquez sur `Appliquer` quand vous serez satisfait du résultat, et refermez la fenêtre de l'histogramme.

#### On va supprimer le bruit vert...

Cette fonction est équivalente au fameux filtre `HLVG` qu'on trouve sous forme de plugin Photoshop. Normalement, si vous avez effectué l'ajustement des couleurs par photométrie, cette action ne devrait pas changer grand-chose à votre image.
* Cliquez sur le menu `Traitement de l'image`, puis sur `Suppression du bruit vert (SCNR)...` : {{<figure src="menu-scnr.fr.png">}}
* Conservez les valeurs par défaut, puis cliquez sur `Appliquer` : {{<figure src="scnr-dialog.fr.png">}}
* Observez le résultat au niveau de la fenêtre d'affichage couleur de l'image : {{<figure src="scnr-image.jpeg">}}
* Refermez la fenêtre de réduction du bruit vert.

#### On va monter un peu la saturation...

* Dans le menu Traitement de l'image, cliquez sur `Saturation des couleurs...` : {{<figure src="menu-satu.fr.png">}}
* Et choisissez une valeur entre 0.20 et 0.50, selon les goûts de chacun : {{<figure src="satu-dialog.fr.png">}}
* Le curseur `Facteur de fond de ciel` permet de définir la protection du fond de ciel. Plus la valeur est haute, et moins la saturation est appliquée au fond de ciel.
* Observez le changement dans la fenêtre de visualisation couleur : {{<figure src="satu-image.jpeg">}}
* Cliquez sur `Appliquer` quand vous êtes satisfait du résultat et refermez la fenêtre de saturation.

#### Et enfin on va sauvegarder notre image...

En fonction du traitement ultérieur que vous allez effectuer à votre image, choisissez le format de fichier adapté.
* Par exemple si vous allez reprendre votre image sous un autre logiciel Astro ou même sous `GIMP`, vous pouvez la sauver en `FITS`.
* Si vous allez reprendre l'image sous Photoshop, sauvez-la plutôt en `TIFF`.
* Si vous souhaitez la publier directement telle quelle, sauvez-là alors en `JPG` ou en `PNG`.

Il existe 2 méthodes pour sauvegarder votre image.
1. Méthode 1 (plus simple, non valable pour une image noir et blanc) :
    * Faites un clic-droit dans l'image RVB puis cliquez sur `Enregistrer l'image RVB en...` : {{<figure src="image-save.fr.jpeg">}}
    * Dans le cas présent, je vais m'arrêter là et donc sauver mon image en JPG pour la publier ci-dessous.
    * Donnez un nom à l'image, puis ajustez la qualité de compression du JPG et cliquez sur `Enregistrer` : {{<figure src="save-jpg.fr.png">}}
1. Méthode 2 (plus complète) :
    * Cliquez sur l'icône `Enregistrer sous` comme ci-dessous : {{<figure src="menu-save-as.fr.png">}}
    * Cliquez sur `Fichiers image pris en charge` puis sélectionnez le format désiré (vous pouvez aussi rester sur `Fichiers image pris en charge` et indiquer le format avec l'extension dans le nom de fichier) : {{<figure src="save-as-jpg.fr.png">}}
    * Donnez un nom à votre image et cliquez sur `Enregistrer` (vous pouvez aussi changer le dossier de destination si besoin) : {{<figure src="save-as-jpg-2.fr.png">}}
    * Ajustez la qualité de compression du `JPG` et cliquez sur `Enregistrer` : {{<figure src="jpg-quality.fr.png">}}

#### C'est fini !!!

Pour finir, je vous montre quand même l'image finale ?

Tout d'abord pour rappel la brute à gauche et l'image finale à droite, empilement de 15 brutes de 180s. Pas trop mal pour seulement 45 minutes de pose, non ? Vous pouvez cliquer [ici](http://www.astrosurf.com/colmic/Traitement_SiriL/brutes/M8-M20_FSQ106_Red073x_ASI2600_15x180s.jpg) pour obtenir la full :
{{<figure src="bruteM8M20.jpeg">}}
{{<figure src="M8-M20_FSQ106_Red073x_ASI2600_15x180s.jpg">}}
Voilà, le traitement de cette première image couleur sous Siril est terminé, on a déjà quelque chose de sympa à regarder qui satisfera sans doute nombre d'entre vous. 
    

---
title: "Tutoriels"
author: Cyril Richard
menu: "main"
weight: 4
---

## Tutoriels Externes
* [Traitement d'un paysage nocturne avec Siril (anglais)](https://pixls.us/articles/processing-a-nightscape-in-siril/)

## Chaines YouTube
* [Chaine officielle de Siril](https://www.youtube.com/channel/UC_8UVzay-xlds4pjyRhHUOw)
* [L'astrophotographie à l'APN](https://www.youtube.com/channel/UCL6_FyoBsia3FlnfdSDA0CA)

## Cours en Ligne
* https://www.learn.siril.org/

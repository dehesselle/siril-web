---
title: First steps -- pre-processing with scripts
author: Cyril Richard
show_author: true
featured_image: Capture-05.png
rate: "0/5"
type: page
---

This tutorial explores your first steps with Siril and its scripting capability.

Siril comes with a few basic scripts. They allow you to preprocess your images in one click!

##### 1. Create the folders to hold your images types.

Create the following 4 directories named as:
 * `biases`
 * `darks`
 * `flats`
 * `lights`

##### 2. Put your RAW images in the directories created in the previous step.

*Attention*: don’t mix other files, even JPEG (or other formats), with your RAW files: Siril will take all the files inside the directories as input files for processing.

##### 3. Click on the *home* button and navigate to your project folder .

This is tell Siril where all your images are located.

##### 4. Click the *Open* button.

{{< figure src="Capture-02.png" caption="Headerbar of Siril" >}}

Siril will validate your working directory.

##### 5. Click on the *Scripts* button and select the script of your choice.

The main script that covers basic needs is named `OSC_preprocessing`. We recommend to use this one over the Drizzle version.

If you don’t see the button, do the follwoing:
1. Go to your preferences (`ctrl` + `P`)
2. Remove all script paths (as shown in the following screenshot).

  {{< figure src="Capture-03.png" caption="Preferences dialog box. Script paths can be added here." >}}

3. Validate your changes.
4. Close and restart Siril.

   You are now ready to run a script.

If you need more scripts:
1. Click on the *hamburger* menu and on “Get Scripts”.

   Script files are text files with `ssf` extension.
2. Download a script.

   You can download any script you want, put it in any folder. You just have to indicate to Siril the path where it needs to search for it. Do it in Preferences, in the same tab as seen in the illustration above.

   {{< figure src="Capture-04.png" caption="Main menu of Siril." >}}

Depending of the number of RAW images and the power of your computer, Siril can take some time to process your images. At the end of the script, you should have a window like that:

{{< figure src="Capture-05.png" caption="Script is running." >}}

Your images have been calibrated, aligned (registered) and stacked. Congratulations! It is now time to process the result in Siril, and please, don’t be afraid if your stacking result is too dark and/or too green at this stage, this is normal ;).

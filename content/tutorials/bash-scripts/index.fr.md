---
title: Scripts Siril avancés avec bash ou DOS
author: Cyril Richard
show_author: true
featured_image: bash.png
rate: "4/5"
type: page
---

Pour les inconditionels de la ligne de commande, Siril permet d'intégrer ses scripts (simple suite de commandes Siril) dans des scripts shell (bash ou dos) qui sont bien plus puissants et permettent la gestion de variables. Nous allons voir dans ce mini tutoriel comment réaliser de tels scripts sous une plateforme de type UNIX ainsi que sous Microsoft Windows. L'exemple que nous allons présenter consiste simplement en un rééchantillonnage d'une image bin 2x2 en bin 1x1, en mutlipliant sa taille par deux avec la commande siril `resample`. Un exemple plus complexe montre ensuite comment appliquer des commandes disponibles pour des images seules à toutes les images d'une séquence.

{{< table_of_contents >}}

## Redimensionner une image

### UNIX

L'étape préliminaire consiste à créer le fichier et le rendre exécutable :

```bash
touch resample
chmod a+x resample
```

Editez le fichier et copiez le texte suivant :

```bash
#!/bin/bash
#
#  rééchantillonner un fichier 
#  typiquement pour resampler un bin 2x2 en 1x1
#  utile pour lrvb avec l en 1x1 et rgb en 2x2
#
version=$(siril --version |awk '{print $2}')
ext=fits

siril-cli -i ~/.siril/siril.cfg -s - <<ENDSIRIL >/dev/null 2>&1
requires $version
setext $ext
load $1
resample 2.0
save $1
close
ENDSIRIL
```

Sauvegardez le fichier puis fermez le, il suffit alors d'exécuter le script en tapant 
```
./resample nom_de_fichier
```
ou bien, si les images contiennent le motif `2x2` dans le nom de fichier :

```bash
for i in $(/bin/ls *.fits |grep 2x2); do ./resample $i; done
```

Remarque : pour les systèmes MacOS, le script fonctionne de la même manière, seul le chemin de Siril doit être mis à jour avec quelque chose comme `/Applications/SiriL.app/Contents/MacOS/siril`.

### Microsoft Windows (cmd)

Créer un fichier vide avec la ligne de commande suivante

```cmd
copy NUL resample.bat
```

ou bien sûr via l'interface graphique.

Editez le fichier et copiez le texte suivant :

```cmd
echo OFF
FOR /F "tokens=2 " %%g IN ('siril --version') do (SET version=%%g)
set ext=fits

(
echo requires %version%
echo setext %ext%
echo load %1
echo resample 2.0
echo save %1
echo close
) | "C:\Program Files\SiriL\bin\siril-cli.exe" -s - >nul 2>&1

pause
```

Sauvegardez le fichier puis fermez le. Vous pouvez maintenant exécuter le script en tirant un fichier à redimensionner dessus ou en tapant la commande :

```cmd
resample.bat nom_de_fichier
```

## Appliquer une commande pour image seule à toute une séquence

### UNIX

Créez un fichier exécutable comme montre ci-dessus et copiez le code suivant:

```bash
#!/bin/bash

# Usage
#######
# ./genseqscript.sh command seqname [prefix ext]
# Examples:
###########
#
# Apply a median filter to all images from /Images/siril/process/r_pp_light_.seq with "fit" extension and save them with "med_" prefix
# ./genseqscript.sh "fmedian 5 1" "/Images/siril/process/r_pp_light_.seq" med_ fit
#
# Apply a 90 deg rotation w/o crop to all images from pp_light_.seq located in current folder
# ./genseqscript.sh "rotate 90 -nocrop" "pp_light_" rot90_

# User settings
# command: the command to be applied to each image of the sequence
# seqname: name of the sequence, can be a full path or just a sequence namein the current directory (w or w/o .seq extension)
# prefix: (optional) the prefix to be preprended to the processed file names, def: ""
# ext: (optional) chosen FITS extension, def: fits (can also be fit or fts)

# Default values
prefix=""
ext=fits

# User command line inputs
if [[ $# -lt 2 ]]; then
    echo "Illegal number of parameters - you should pass at least command and sequence name" >&2
    exit 1
fi
if [ ! -z "$1" ]; then command="$1"; fi
if [ ! -z "$2" ]; then seqname="$2"; fi
if [ ! -z "$3" ]; then prefix="$3"; fi
if [ ! -z "$4" ]; then ext="$4"; fi

printf "Command to be run: %s\n" "$command"
printf "squence name: %s\n" $seqname
printf "prefix: %s\n" $prefix
printf "FITS extension: %s\n" $ext

currdir=$(dirname "$seqname")
seqname=$(basename "$seqname")
seqext=".seq"

if [ "$currdir" = "." ]; then currdir=$(pwd); fi
if [ "${seqname: -4}" = $seqext ]; then seqname="${seqname%.*}"; fi
fullseqname="$currdir"/"$seqname"$seqext

printf "Working directory: %s\n" "$currdir"
printf "Sequence to be processed: %s%s\n" "$seqname" $seqext

[[ -f "$currdir"/"$seqname"$seqext ]] || { echo "The specified sequence does not exist - aborting"; exit 1; }

# get siril version
version=$(siril-cli --version |awk '{print $2}')

while read line
do
  if [[ "$line" =~ ^I.* ]]; then
    spec=($line)
    currframenb=${spec[1]}
    currframe=$(printf "$seqname%05d.$ext" $((currframenb)))
    savename=$prefix"$currframe"
    # check first frame exists, otherwise break
    [[ -f "$currdir"/"$currframe" ]] || { printf "First file %s does not exist... check if the .seq file is valid or the selected FITS extension ('%s' defined here) matches your files - aborting\n" "$currframe" $ext; exit 1; }
    
    printf "processing file: %s\n" "$currframe"
    siril-cli -s - <<ENDSIRIL >log 2>&1
requires $version
setext $ext
cd "$currdir"
load "$currframe"
$command
save "$savename"
ENDSIRIL
  fi
done < "$currdir"/"$seqname"$seqext
```
Enregistrez-le sous le nom genseqscript.sh

Lancez le script en tapant par exemple:

```
./genseqscript.sh "fmedian 5 1" "/Images/siril/process/r_pp_light_.seq" med_ fit
```
Cela appliquera un filtre médian à toutes les images de la séquence /Images/siril/process/r_pp_light_.seq qui ont une extension "fit" en les préfixant avec "med_". Vous pouvez, bien entendu, passer n'importe quelle autre commande (valide) de votre choix. [Cette page](https://free-astro.org/index.php?title=Siril:Commands) liste l'ensemble des commandes Siril et pourra vous fournir de précieux exemples. Avant d'exécuter une commande avec ce script, pensez à vérifier que son équivalent avec le préfixe `seq` n'existe pas déjà. Il sera plus rapide à exécuter directement grâce à la parallélisation.

### Microsoft Windows (Powershell)

La même chose peut être écrite sous Microsoft Windows en utilisant, par exemple, du Powershell. Créez un fichier genseqscript.ps1 et copiez le code suivant:

```powershell
# Usage
#######
# .\genseqscript.ps1 command seqname [prefix ext]
# Examples:
###########
#
# Apply a median filter to all images from C:\MyImages\r_pp_light_.seq with "fit" extension and save them with "med_" prefix
# .\genseqscript.ps1 "fmedian 5 1" "C:\MyImages\r_pp_light_.seq" med_ fit
#
# Apply a 90 deg rotation w/o crop to all images from pp_light_.seq located in current folder
# .\genseqscript.ps1 "rotate 90 -nocrop" pp_light_ rot90_

# User settings
# command: the command to be applied to each image of the sequence. Enclosed in double quotes if there is more than one word
# seqname: name of the sequence, can be a full path or just a sequence namein the current directory (w or w/o .seq extension). Enclosed in double quotes if there are spaces in the path
# prefix: (optional) the prefix to be preprended to the processed file names, def: ""
# ext: (optional) chosen FITS extension, def: fits (can also be fit or fts)

# User command line inputs if any
param ($command,$seqname,$prefix="",$ext="fits" )
"Command to be run: {0:s}" -f $command
"prefix: {0:s}" -f $prefix
"FITS extension: {0:s}" -f $ext

If (Split-Path -Path $seqname -IsAbsolute) {
    $currdir = Split-Path -Path $seqname
    $seqname = Split-Path -Path $seqname -Leaf
}
Else {
    $currdir = (Get-Location).path
}

$seqext = [IO.Path]::GetExtension($seqname)
If ($seqext.Length -eq 0){
    $seqext = '.seq'
}
Else {
    $seqname = [IO.Path]::GetFileNameWithoutExtension($seqname)
}

"Working directory: {0:s}" -f $currdir
"Sequence to be processed: {0:s}{1:s}" -f $seqname,$seqext
If (-Not (Test-Path -Path $currdir\$seqname$seqext -PathType Leaf )){
    "The specified sequence does not exist - aborting"
    Exit
}

# path to siril-cli
$sirilcliexe="C:\Program Files\SiriL\bin\siril-cli.exe"

# get siril version
$version=($(& $sirilcliexe --version) -split ' ')[-1]
$log="{0:s}\log.txt"-f $currdir

Get-Content $currdir\$seqname$seqext | Select-String -Pattern '^I ' | ForEach-Object {

  $currframenb=[int]($_ -split ' ')[1]
  $currframe="{0:s}{1:00000}.{2:s}" -f "$seqname",$currframenb,$ext
  # check first frame exists, otherwise break
  If (-Not (Test-Path -Path "$currdir\$currframe" -PathType Leaf )){
    "First file {0:s} does not exist... check if the .seq file is valid or the selected FITS extension ('{1:s}' defined here) matches your files - aborting" -f $currframe,$ext
    Exit
  }
  "processing file: {0:s}" -f "$currframe"
  $savename=$prefix+"$currframe"

  @"
requires $version
setext $ext
cd "$currdir"
load "$currframe"
$command
save "$savename"
"@ | & $sirilcliexe -s - >$log 2>&1
}
```
Lancez le script dans une console Powershell en tapant:

```
.\genseqscript.ps1 "fmedian 5 1" "C:\MyImages\r_pp_light_.seq" med_ fit
```

Cela appliquera un filtre médian a toutes les images de la séquence C:\MyImages\r_pp_light_.seq qui ont une extension "fit" en les préfixant avec "med_". Vous pouvez, bien entendu, passer n'importe quelle autre commande (valide) de votre choix. [Cette page](https://free-astro.org/index.php?title=Siril:Commands) liste l'ensemble des commandes Siril et pourra vous fournir de précieux exemples. Avant d'exécuter une commande avec ce script, pensez à vérifier que son équivalent avec le préfixe `seq` n'existe pas déjà. Il sera plus rapide à exécuter directement grâce à la parallélisation.

## Conclusion

Ces exemples montrent qu'il est possible de scripter n'importe quelle étape de votre traitement en combinant la puissance de scripts shell et celle de Siril. La seule limite reste votre imagination.

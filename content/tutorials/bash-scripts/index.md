---
title: Advanced Siril scripting with bash or DOS
author: Cyril Richard
show_author: true
featured_image: bash.png
rate: "4/5"
type: page
---

For fans of the command line, Siril allows you to integrate its scripts (a simple suite of Siril commands) into shell scripts (bash or dos) which are much more powerful and allow the management of variables. We will see in this tutorial how to create such scripts on a UNIX type platform as well as on Microsoft Windows. The first example we are going to present consists of simply resampling a 2x2 bin image into a 1x1 bin, multiplying its size by two with the siril `resample` command. A more complex example afterwards shows how to apply commands that are only available to single images to all the images of a sequence.

{{< table_of_contents >}}

## Resampling an image

### UNIX

The preliminary step is to create the file and make it executable:

```bash
touch resample
chmod a+x resample
```

Edit the file and copy the following text:

```bash
#!/bin/bash
#
#  resample a file
#  used to resample bin 2x2 to 1x1
#  useful for lrgb with l in 1x1 and rgb in 2x2
#
version=$(siril --version |awk '{print $2}')
ext=fits

siril-cli -i ~/.siril/siril.cfg -s - <<ENDSIRIL >/dev/null 2>&1
requires $version
setext $ext
load $1
resample 2.0
save $1
close
ENDSIRIL
```
Save the file then close it, then all you have to do is run the script by typing
```
./resample filename
```
or, if the images contain the pattern `2x2` in the filename:

```bash
for i in $(/bin/ls *.fits |grep 2x2); do ./resample $i; done
```

Note: for MacOS systems, the script work the same way, only the siril path must be updated with something like `/Applications/SiriL.app/Contents/MacOS/siril`.

### Microsoft Windows (cmd)

Create an empty file with the following command line

```cmd
copy NUL resample.bat
```

or of course through the UI.

Edit the file and copy the following text:

```cmd
echo OFF
FOR /F "tokens=2 " %%g IN ('siril --version') do (SET version=%%g)
set ext=fits

(
echo requires %version%
echo setext %ext%
echo load %1
echo resample 2.0
echo save %1
echo close
) | "C:\Program Files\SiriL\bin\siril-cli.exe" -s - >nul 2>&1

pause
```
Save the file then close it. You can now run the script either by dragging a picture onto its icon or by typing the command :

```cmd
resample.bat filename
```

## Apply a single-image command to a sequence

### UNIX

Create an executable file as shown above and copy the following:

```bash
#!/bin/bash

# Usage
#######
# ./genseqscript.sh command seqname [prefix ext]
# Examples:
###########
#
# Apply a median filter to all images from /Images/siril/process/r_pp_light_.seq with "fit" extension and save them with "med_" prefix
# ./genseqscript.sh "fmedian 5 1" "/Images/siril/process/r_pp_light_.seq" med_ fit
#
# Apply a 90 deg rotation w/o crop to all images from pp_light_.seq located in current folder
# ./genseqscript.sh "rotate 90 -nocrop" "pp_light_" rot90_

# User settings
# command: the command to be applied to each image of the sequence
# seqname: name of the sequence, can be a full path or just a sequence namein the current directory (w or w/o .seq extension)
# prefix: (optional) the prefix to be preprended to the processed file names, def: ""
# ext: (optional) chosen FITS extension, def: fits (can also be fit or fts)

# Default values
prefix=""
ext=fits


# User command line inputs
if [[ $# -lt 2 ]]; then
    echo "Illegal number of parameters - you should pass at least command and sequence name" >&2
    exit 1
fi
if [ ! -z "$1" ]; then command="$1"; fi
if [ ! -z "$2" ]; then seqname="$2"; fi
if [ ! -z "$3" ]; then prefix="$3"; fi
if [ ! -z "$4" ]; then ext="$4"; fi

printf "Command to be run: %s\n" "$command"
printf "squence name: %s\n" $seqname
printf "prefix: %s\n" $prefix
printf "FITS extension: %s\n" $ext

currdir=$(dirname "$seqname")
seqname=$(basename "$seqname")
seqext=".seq"


if [ "$currdir" = "." ]; then currdir=$(pwd); fi
if [ "${seqname: -4}" = $seqext ]; then seqname="${seqname%.*}"; fi
fullseqname="$currdir"/"$seqname"$seqext

printf "Working directory: %s\n" "$currdir"
printf "Sequence to be processed: %s%s\n" "$seqname" $seqext

[[ -f "$currdir"/"$seqname"$seqext ]] || { echo "The specified sequence does not exist - aborting"; exit 1; }

# get siril version
version=$(siril-cli --version |awk '{print $2}')

while read line
do
  if [[ "$line" =~ ^I.* ]]; then
    spec=($line)
    currframenb=${spec[1]}
    currframe=$(printf "$seqname%05d.$ext" $((currframenb)))
    savename=$prefix"$currframe"
    # check first frame exists, otherwise break
    [[ -f "$currdir"/"$currframe" ]] || { printf "First file %s does not exist... check if the .seq file is valid or the selected FITS extension ('%s' defined here) matches your files - aborting\n" "$currframe" $ext; exit 1; }
    
    printf "processing file: %s\n" "$currframe"
    siril-cli -s - <<ENDSIRIL >log 2>&1
requires $version
setext $ext
cd "$currdir"
load "$currframe"
$command
save "$savename"
ENDSIRIL
  fi
done < "$currdir"/"$seqname"$seqext
```
Save it as genseqscript.sh

Just run the script by typing for example:

```
./genseqscript.sh "fmedian 5 1" "/Images/siril/process/r_pp_light_.seq" med_ fit
```

This will apply a median filter to all images from /Images/siril/process/r_pp_light_.seq with "fit" extension and save them with "med_" prefix. You can, of course, pass any (valid) command of your liking. Browse through [this page](https://free-astro.org/index.php?title=Siril:Commands) to find out more. Also check first a `seq` equivalent for the transformation you wish to apply does not exist already as it will execute faster thanks to parrallelization.

### Microsoft Windows (Powershell)

The same can be done on Microsoft Windows using for instance Powershell (instead of cmd as shown above). Create the file genseqscript.ps1 and copy the following:

```powershell
# Usage
#######
# .\genseqscript.ps1 command seqname [prefix ext]
# Examples:
###########
#
# Apply a median filter to all images from C:\MyImages\r_pp_light_.seq with "fit" extension and save them with "med_" prefix
# .\genseqscript.ps1 "fmedian 5 1" "C:\MyImages\r_pp_light_.seq" med_ fit
#
# Apply a 90 deg rotation w/o crop to all images from pp_light_.seq located in current folder
# .\genseqscript.ps1 "rotate 90 -nocrop" pp_light_ rot90_

# User settings
# command: the command to be applied to each image of the sequence. Enclosed in double quotes if there is more than one word
# seqname: name of the sequence, can be a full path or just a sequence namein the current directory (w or w/o .seq extension). Enclosed in double quotes if there are spaces in the path
# prefix: (optional) the prefix to be preprended to the processed file names, def: ""
# ext: (optional) chosen FITS extension, def: fits (can also be fit or fts)

# User command line inputs if any
param ($command,$seqname,$prefix="",$ext="fits" )
"Command to be run: {0:s}" -f $command
"prefix: {0:s}" -f $prefix
"FITS extension: {0:s}" -f $ext

If (Split-Path -Path $seqname -IsAbsolute) {
    $currdir = Split-Path -Path $seqname
    $seqname = Split-Path -Path $seqname -Leaf
}
Else {
    $currdir = (Get-Location).path
}

$seqext = [IO.Path]::GetExtension($seqname)
If ($seqext.Length -eq 0){
    $seqext = '.seq'
}
Else {
    $seqname = [IO.Path]::GetFileNameWithoutExtension($seqname)
}

"Working directory: {0:s}" -f $currdir
"Sequence to be processed: {0:s}{1:s}" -f $seqname,$seqext
If (-Not (Test-Path -Path $currdir\$seqname$seqext -PathType Leaf )){
    "The specified sequence does not exist - aborting"
    Exit
}

# path to siril-cli
$sirilcliexe="C:\Program Files\SiriL\bin\siril-cli.exe"

# get siril version
$version=($(& $sirilcliexe --version) -split ' ')[-1]
$log="{0:s}\log.txt"-f $currdir

Get-Content $currdir\$seqname$seqext | Select-String -Pattern '^I ' | ForEach-Object {

  $currframenb=[int]($_ -split ' ')[1]
  $currframe="{0:s}{1:00000}.{2:s}" -f "$seqname",$currframenb,$ext
  # check first frame exists, otherwise break
  If (-Not (Test-Path -Path "$currdir\$currframe" -PathType Leaf )){
    "First file {0:s} does not exist... check if the .seq file is valid or the selected FITS extension ('{1:s}' defined here) matches your files - aborting" -f $currframe,$ext
    Exit
  }
  "processing file: {0:s}" -f "$currframe"
  $savename=$prefix+"$currframe"

  @"
requires $version
setext $ext
cd "$currdir"
load "$currframe"
$command
save "$savename"
"@ | & $sirilcliexe -s - >$log 2>&1
}
```

Just run the script by typing in a powershell console:

```
.\genseqscript.ps1 "fmedian 5 1" "C:\MyImages\r_pp_light_.seq" med_ fit
```

This will apply a median filter to all images from C:\MyImages\r_pp_light_.seq with "fit" extension and save them with "med_" prefix. You can, of course, pass any (valid) command of your liking. Browse through [this page](https://free-astro.org/index.php?title=Siril:Commands) to find out more. Also check first a `seq` equivalent for the transformation you wish to apply does not exist already as it will execute faster thanks to parrallelization.

## Conclusion

These examples show that it is possible to script every step of the image processing steps using the power of shell scripts coupled with that of Siril. The only limit is your imagination.

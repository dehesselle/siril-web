---
title: Travailler avec les comètes
author: Cecile Melis
show_author: true
featured_image: cover.png
rate: "3/5"
type: page
---

Ce tutoriel a pour but d'aborder un sujet qui revient périodiquement, à savoir le traitement des images de comètes. Il vous montrera comment réaliser des images fixes, alignées sur les étoiles et/ou la comète elle-même, des animations simples ou des animations plus complexes en utilisant le "superstacking". Le tout, avec un étalonnage photométrique automatiquement appliquée à toutes les images afin que les couleurs soient bien équilibrées.

{{< table_of_contents >}}

## Images fixes

Nous allons commencer ce tutoriel avec une séquence de brutes prétraitées. Si vous avez besoin d'apprendre comment faire cela, vous pouvez vous référer au tutoriel [traitement manuel](../tuto-manual) ou au tutoriel [traitement par scripts](../tuto-scripts). A moins que vous n'ayez modifié les valeurs par défaut, la séquence devrait être nommée `pp_brute_.seq`. Si nécessaire, vous pouvez également procéder à [l'extraction de gradient sur les images unitaires](../../faq/#comment-supprimer-un-gradient-de-fond-de-ciel-complexe-) de votre séquence avant de commencer ce tutoriel, dans ce cas, vous utiliserez une séquence nommée `bkg_pp_brute_.seq`.

Note : la séquence d'entrée doit être faite d'images FITS individuelles, pas de SER ni de FITSEQ, afin d'appliquer la balance des couleurs.

Les bases du traitement des comètes sont les suivantes :
- aligner la séquence sur les étoiles
- aligner la séquence sur la comète
- empiler les images fixes, soit sur les étoiles, soit sur la comète.
- mélanger les deux résultats (en dehors de Siril)

### Choisir une bonne référence

C'est une étape cruciale car elle va déterminer le cadrage final de vos images. Cela dépendra de votre longueur focale, de l'utilisation d'une monture et du passage de la comète dans le champ, de la façon dont la comète remplit ou non le champ de vision, etc... Choisissez l'image où la position de la comète est telle que vous voulez qu'elle soit dans l'image finale. Si votre monture ne suit pas, vous voudrez probablement choisir une image vers le milieu de la session afin de maximiser le chevauchement avec toutes les autres images de la séquence.

Vous pouvez parcourir toutes les images à l'aide du sélecteur d'images. Une fois que vous avez fait votre choix, sélectionnez-la comme référence pour la séquence et notez son numéro.

{{<figure src="refselection.png" link="refselection.png" caption="Mettez l'image en surbrillance et cochez la case **Image de référence**. Dans l'exemple ci-dessus, l'image n°10 est sélectionnée.">}}

### Étalonner les couleurs de votre image de référence

C'est quelque chose que vous ne feriez normalement pas à ce stade, mais c'est ainsi que nous obtiendrons les couleurs correctes pour l'empilement lié à la comète et pour les animations. Ouvrez l'image de référence seule (ne l'ouvrez pas à partir de la séquence). Dans notre exemple, nous avons choisi `bkg_pp_brute_00010.fit`. Vous pouvez le faire via l'interface utilisateur (bouton `Ouvrir`) ou en tapant `load bkg_pp_brute_00010.fit` dans la ligne de commande.

Ouvrez le menu `Traitement de l'image`, puis `Étalonnage des couleurs` et `Étalonnage des couleurs par photométrie`. Remplissez les informations requises et validez. Une fois l'astrométrie résolue et l'image étalonnée, n'oubliez pas de sauvegarder les changements, soit par l'interface utilisateur (bouton `Enregistrer`), soit en tapant `save bkg_pp_brute_00010.fit` dans la ligne de commande. Cela va écraser l'image existante, ce qui est ce que nous voulons.
{{<figure src="pCC.png" link="pCC.png" caption="Étalonner l'image avec la photométrie pour révéler la belle teinte verte autour du noyau.">}}

### Alignement global et empilement

Rechargez la séquence. Elle devrait s'ouvrir sur l'image de référence possédant la couleur équilibrée. Si ce n'est pas le cas, soit vous avez étalonné la mauvaise image, soit vous avez oublié d'enregistrer vos modifications. **N'allez pas plus loin avant d'avoir réglé ce problème**. Rendez-vous ensuite dans l'onglet "Alignement". Alignez la séquence avec `Alignement global (ciel profond)`.

À la fin de l'étape d'alignement, Siril choisira normalement l'image avec la plus petite FWHM comme nouvelle image de référence. Bien que cela soit valable pour le cas général, nous voulons forcer l'utilisation de notre propre image de référence. Donc ré-ouvrez le sélecteur d'image et définissez à nouveau votre propre référence définie plus tôt.

Allez dans l'onglet `Empilement` et sélectionnez l'algorithme de rejet `Windsorized Sigma Clipping`. Vous pouvez diminuer les sigmas, en particulier le sigma élevé pour rejeter autant que possible la comète. Plus important encore, sélectionnez la normalisation `Additive avec mise à l'échelle`, car c'est cela qui va homogénéiser les couleurs. Cependant, ne cochez pas la case `Normalisation en sortie`. Empilez vos images. Le résultat empilé devrait avoir des couleurs équilibrées puisque toutes les images ont été normalisées pour correspondre à l'image de référence.

Il se peut que vous ne puissiez pas supprimer toute la comète, en particulier si elle ne s'est pas beaucoup déplacée au cours de votre session. Elle peut encore être retirée lors de la composition avec l'empilement centré sur la comète.

### Préparer l'animation globale (étoiles fixes)
Une dernière étape si vous souhaitez faire une animation, avec des étoiles fixes et la comète en mouvement (il faut absolument le faire **avant** de passer à l'alignement sur la comète):
- Allez dans l'onglet `Séquence`.
- Rechargez la séquence alignée
- Si vous souhaitez avoir une taille de sortie spécifique, utilisez la commande `boxselect`.

  Par exemple, j'aimerais avoir une animation carrée de 1600x1600 (garder un multiple de 32 est généralement une bonne idée). Je peux taper `boxselect 0 0 1600 1600` dans la ligne de commande, ce qui dessinera une boîte de la taille désirée dans le coin supérieur gauche. Je peux ensuite faire glisser la sélection où je veux. J'ai choisi de la positionner avec la comète à environ un tiers du bord droit pour garder une bonne partie de la queue. Si je veux connaître la position finale de la boîte pour une utilisation future, taper à nouveau `boxselect` affichera sa position [x y largeur hauteur] dans l'onglet `Console`.
- Dans la partie exportation de l'onglet `Séquence`, tapez un nom pour la séquence de sortie. Ici, nous allons utiliser `global`. Cochez la case `Normaliser les images`, sélectionnez `FITS` et appuyez sur le bouton `Exporter la séquence`. Cela va appliquer en une seule fois le recadrage (grâce à la sélection dessinée) et la balance des couleurs de l'image de référence (grâce à la normalisation) à toute la séquence.

{{<figure src="globalcrop.png" link="globalcrop.png" caption="Dessinez une sélection sur votre image et exportez la séquence avec la normalisation active, pour appliquer la balance des couleurs de l'image de référence à toutes les images.">}}

Si vous ouvrez la séquence `global_`, vous verrez que toutes les images ont les couleurs équilibrées.

### Alignement et empilement sur la comète

Nous allons maintenant aligner la séquence sur la comète :
- Chargez à nouveau la séquence alignée `r(_bkg)_pp_brute_` et allez dans l'onglet `Alignement`.
- Sélectionnez la méthode `Alignement Comète/Astéroïde`.
- Utilisez le sélecteur d'image pour charger la première image de la séquence.
- Dessinez une sélection autour de la comète et cliquez sur `Pointer objet dans #1`.
- Utilisez le sélecteur d'image pour charger la dernière image de la séquence.
- Dessinez une sélection autour de la comète et cliquez sur `Pointer objet dans #2`.

A la fin de ce processus, Siril devrait afficher un vecteur vitesse en X et Y. Assurez-vous que la case `Cumul données de reg.` est cochée et cliquez sur `Aligner`.
{{<figure src="cometreg.png" link="cometreg.png" caption="Sélectionnez la comète dans la première et la dernière image de la séquence pour ajouter les données d'alignement de la comète au fichier .seq.">}}

- Allez sur l'onglet `Empilement`.
- Sélectionnez `Additive avec mise à l'échelle` pour la méthode de normalisation.
- Sélectionnez `Windsorized Sigma Clipping` pour l'algorithme de rejet. Vous pouvez essayer différentes valeurs de sigma. Des valeurs plus faibles rejetteront plus de pixels et supprimeront plus de traînées d'étoiles (avec probablement plus de bruit), tandis que des valeurs plus élevées réduiront le bruit mais pourront conserver plus de traînées d'étoiles visibles.
- N'oubliez pas de changer le nom de l'image empilée pour chaque test de réglage que vous faites avec les sigmas. Sinon, les images seront écrasées.

{{<figure src="comet1.png" link="comet1.png" caption="Vue de l'empilement de comètes avec sigma hi/lo=1">}}
{{<figure src="comet3.png" link="comet3.png" caption="Vue de l'empilement de comètes avec sigma hi/lo=3">}}

L'image avec un sigma plus élevé montre plus de détails dans la queue de la comète mais aussi plus de traînées d'étoiles. Les deux images ont une bonne balance des couleurs.

### Préparer l'animation de la comète fixe
Une dernière étape si vous souhaitez faire une animation, avec la comète fixe et les étoiles en mouvement :
- Allez dans l'onglet `Séquence`.
- Rechargez la séquence alignée.
- Si vous souhaitez avoir une taille de sortie spécifique, utilisez la commande `boxselect`.

  Si vous avez noté la sélection pour l'exportation [étoiles fixes](#préparer-lanimation-globale-étoiles-fixes), vous pouvez utiliser la même position pour reproduire le même cadrage. Par exemple, si je tape la commande `boxselect 1153 133 1600 1600`, la sélection active correspond à la sélection précédemment effectuée pour exporter la séquence `global_`.
- Dans la partie exportation de l'onglet `Séquence`, tapez un nom pour la séquence de sortie. Ici, nous allons utiliser `comet`. Cochez la case `Normaliser les images`, sélectionnez `FITS` et appuyez sur le bouton `Exporter la séquence`.

Si vous ouvrez la séquence `comet_`, vous verrez que toutes les images sont équilibrées en couleur.

### Combiner les images globales et celles de la comète

Vous pouvez souhaiter combiner les 2 images globales ensemble. Avant de passer à votre outil d'édition d'image préféré ([GIMP](https://www.gimp.org/) dans mon cas), il y a encore quelques étapes que vous pouvez faire dans Siril :
- Dans l'onglet `Conversion`, chargez les empilement d'étoiles fixes et comète fixe.
- Convertissez-les en une séquence, disons, `globalcomet`.
- Sur la première image, dessinez une sélection pour ne conserver que la partie qui vous intéresse.
- Faites un clic droit et sélectionnez `Recadrer Séquence...`.
- Passez maintenant en mode de prévisualisation `Linéaire` et assurez-vous que le curseur du haut est réglé sur 65535.
- Ouvrez l'outil `Transformation de l'histogramme`.
- Déplacez les curseurs `basses lumières` et `tons moyens` au niveau d'étirement désiré.
- Cochez la case `Appliquer à la séquence` en bas.
- Vous aurez 2 images de niveau identique et étirées, pour composer votre image finale.

Ce dernier paragraphe termine la section des images fixes de ce tutoriel. Passons maintenant à la partie animation.

## Animations

Pour être honnête, lorsque vous êtes arrivé à cette étape, le plus gros du travail a déjà été fait. Les seules étapes qui restent sont la mise en forme des images et l'exportation d'une vidéo.

Nous allons voir comment cela fonctionne sur la séquence `global_` exportée [ici](#préparer-lanimation-globale-étoiles-fixes), mais la même chose peut être appliquée sur la séquence `comet_` exportée [ici](#préparer-lanimation-de-la-comète-fixe) :
- Chargez la séquence `global_` dans l'onglet `Séquence`.
- Si ce n'est pas déjà le cas, passez en mode de prévisualisation `Linéaire` et assurez-vous que le curseur du haut est réglé sur 65535.
- Ouvrez l'outil `Transformation de l'histogramme`.
- Déplacez les curseurs `basses lumières` et `tons moyens` au niveau d'étirement désiré.
- Cochez la case "Appliquer à la séquence" en bas.

Vous avez maintenant une séquence étirée, `mtf_global_`, avec toutes les images correspondant aux couleurs, aux niveaux et à l'étirement.
Dans l'onglet `Séquence`, vous pouvez exporter une vidéo :
- Sélectionnez un des formats d'exportation vidéo (AVI non compressé, MP4(H264/5) ou WEBM).
- Définissez la qualité et le nombre d'images par seconde souhaités.
- Vous pouvez décocher la case `Normaliser les images`, ce n'est plus nécessaire et cela accélérera le processus.
- Cliquez sur "Exporter la séquence".

Vous pouvez suivre le même processus avec la séquence `comet_` (étirement et exportation) :

<video controls width=100%>
  <source src="comet_anim.webm" type="video/webm">
Votre navigateur ne prend pas en charge la balise vidéo.
</video> 

Si vous souhaitez retravailler les images avant de créer l'animation, vous pouvez, bien sûr, exporter la séquence comme une série d'images TIFF et les traiter par lots. Personnellement, j'aime aussi les exporter en tant que fichier SER, ajuster la saturation avec [SER Player](https://sites.google.com/site/astropipp/ser-player) et réexporter une vidéo à partir de là.

## Animations avec "superstacks"

**Avertissement** : Cette dernière section est destinée aux utilisateurs plus avancés, à l'aise avec des langages de scripts, tel que Python ou Shell.

Elle montre comment faire de multiples empilements "glissants", appelés "superstacks" par la suite, pour améliorer les détails cométaires et réduire le bruit dans vos animations. Cette technique ne convient pas à tous les jeux de données. Elle convient bien aux sessions avec de petits mouvements d'étoiles entre chaque image, qui dépendent à leur tour de la distance focale, du temps d'exposition de chaque image et de la vitesse apparente de la comète dans le ciel.

Pour illustrer le principe, supposons que nous ayons pris 20 images de la comète et que nous ayons revu les images de la comète fixe (obtenues à partir d'[ici](#préparer-lanimation-de-la-comète-fixe)) pour inspecter le mouvement des étoiles d'une image à l'autre. Ici, la quantité de déplacement des étoiles est assez lente pour regrouper les sous-images 3 par 3. Nous voulons donc automatiser ce processus d'empilement glissant pour :
- sélectionner chaque groupe de 3 images contiguës.
- empiler ce groupe.
- se déplacer d'une image et recommencer.

Jusqu'à ce que nous atteignions la fin de la séquence. Ceci est illustré ci-dessous :

{{<figure src="superstack.png" link="superstack.png" caption="Principe de superstack, avec total=20, nbframes=3 et step=1">}}

Pour des séries plus longues avec moins de mouvements, on pourrait décider de "superstacker" par groupe de 10 images avec un pas de 5 entre chaque superstack. Cela dépendra vraiment des données que vous avez en main et du temps de traitement que vous êtes prêt à passer sur le traitement.

Nous pourrions également décider d'appliquer le même processus sur la séquence d'étoiles fixes.

En pratique, cela peut être facilement scripté en utilisant bash (OS Unix), powershell (Windows) comme décrit dans ce [tutoriel](../bash-scripts) ou mon choix préféré, [Python](https://www.python.org/), avec l'aide du wrapper [pySiril](../pysiril) de version >=0.0.12.

Les scripts nécessaires sont disponibles à cet endroit :
- [bash](scripts/superstack_v0.1.sh)
- [powershell](scripts/superstack_v0.1.ps1)
- [python](scripts/superstack_v0.1.py)

Remarques :
- pour les utilisateurs de macOS, vous devrez donner le chemin complet de siril-cli, qui serait normalement `/Applications/SiriL.app/Contents/MacOS/siril-cli`.
- pour les scripts bash et powershell, vous pouvez optionnellement passer les variables `$nbframes` (par défaut à 3), `$step` (par défaut à 1), le nom de la séquence `$seqname` (par défaut à 'comet_'), le nom du dossier du superstack `$stackfolder` (par défaut à 'superstack'), le nom du dossier de process `$processfolder` (par défaut à 'process') et  l'extension des fits `$ext` (par défaut à 'fit') qui sont positionnels. En Python, tous les paramètres peuvent être passés en tant que kwargs.
- Pour bash et powershell, le script suppose que tous les numéros d'images sont contigus et que la première image porte le numéro 00001. Il n'y a pas de telle restriction dans la version python, bien que ce soit probablement une bonne idée d'avoir une session continue pour éviter les artefacts dans les traînées d'étoiles.

Comment fonctionne le script (avec toutes les valeurs laissées par défaut) ?
- Il doit être exécuté depuis le répertoire de travail qui contient votre sous-dossier `process`.
- Il crée les sous-dossiers `tmp` et `superstack`.
- Il recherche la séquence `comet_`.seq dans le sous-dossier `process`.
- Il crée des liens symboliques vers chaque groupe de 3 sous-dossiers, en incrémentant d'un pas de 1 pour chaque groupe (pour les utilisateurs de Windows, pensez à activer le mode Développeur pour éviter de copier en dur vos fichiers).
- Il exécute un petit script pour empiler (méthode moyenne) sans normalisation (la normalisation a déjà été faite à l'export de la séquence `comet_` donc ceci s'exécute beaucoup plus rapidement).
- Il déplace chaque superstack dans le sous-dossier `superstack` et le numérote en incrémentant à partir de 00001.
- Il supprime le sous-dossier `tmp` quand il a terminé.

Exemple d'utilisation (avec tous les paramètres par défaut):

```cmd
cd C:\myastropics\mybeautifulcomet
python C:\myscripts\superstack.py
```

Exemple de sortie :
```cmd
Number of frames per superstack: 3
Step between each stack: 1
Processing sequence: C:\myastropics\mybeautifulcomet\process\comet_.seq
Superstack saved to folder: superstack
FITS extension: fit

Starting PySiril
WARNING: pysiril uses by default :C:/Program Files/SiriL/bin/siril.exe
INFO   : VERSION siril 1.0.0-rc2 :
INFO   : Siril is compatible with pysiril
INFO   : Initialisation pySiril V0.0.12 : OK

Number of frames in the sequence: 20

Starting Siril
7s 6s 5s 4s 3s 2s 1s
Superstack#1: 1-3
Superstack#2: 2-4
Superstack#3: 3-5
Superstack#4: 4-6
Superstack#5: 5-7
Superstack#6: 6-8
Superstack#7: 7-9
Superstack#8: 8-10
Superstack#9: 9-11
Superstack#10: 10-12
Superstack#11: 11-13
Superstack#12: 12-14
Superstack#13: 13-15
Superstack#14: 14-16
Superstack#15: 15-17
Superstack#16: 16-18
Superstack#17: 17-19
Superstack#18: 18-20
Number of superstacks: 18
```

Essayez ensuite différents paramètres et enregistrez dans un autre dossier de sortie.

```cmd
python C:\myscripts\superstack.py 5 1 stackfolder=superstack2
```
{{<figure src="superstackstill.png" link="superstackstill.png" caption="Effet de superstacks de 3 et 5 images">}}

Une fois le superstack terminé, vous pouvez ouvrir le dossier `superstack` dans Siril, et procéder avec cette nouvelle séquence exactement comme décrit dans la section [animation](#animations). C'est à dire étirer et exporter une vidéo.

L'animation ci-dessous est la même que celle présentée plus haut, mais avec un superstack de 3 sur les données originales.

<video controls width=100%>
  <source src="comet_superstack.webm" type="video/webm">
Votre navigateur ne prend pas en charge la balise vidéo.
</video> 

Et encore, un superstack par groupes de 10. C'est peut-être un peu extrême car les étoiles commencent à avoir une forme allongée, ce qui donne un aspect plus flou au résultat global. 

<video controls width=100%>
  <source src="comet_superstack10.webm" type="video/webm">
Votre navigateur ne prend pas en charge la balise vidéo.
</video>

Enfin, le même procédé a été appliqué à la "star" de la fin d'année passée, C/2021 A1, alias la comète Leonard. C'est le résultat d'un superstack de 10, sur une séquence d'étoiles fixes.

<video controls width=100%>
  <source src="global_superstack10.webm" type="video/webm">
Votre navigateur ne prend pas en charge la balise vidéo.
</video>

Maintenant, vous avez les outils nécessaires pour expérimenter avec votre propre ensemble de données. Profitez-en !


Crédits : Toutes les images utilisées ci-dessus sont fournies par C. Richard. Images 47x60s sur C/2014 Q2 (Jan 2015), sauf les données pour générer la dernière vidéo, qui ont été données avec l'aimable autorisation de R. Ferrieri, alias [@Umberto113](https://twitter.com/Umberto113), 184x30s sur C/2021 A1 (Déc 2021).

---
title: A la découverte de PixelMath
author: Fred Denjean
show_author: true
featured_image: PMwindow.png
rate: "3/5"
type: page
---

Ce tutoriel a pour but de présenter et décrire le nouvel outil de manipulation des images introduit dans la version 1.0.0 de Siril, puis fortement amélioré dans la 1.0.1. Il s'agit de PixelMath.
Cet outil répond aux besoins des utilisateurs de pouvoir manipuler les images lors d'opérations plus ou moins complexes, comme s'ils manipulaient des variables mathématiques.

{{< table_of_contents >}}

## Quelques rappels sur les fichiers images
Les images que nous utilisons en astrophotographie peuvent etre :
- en couleur, on parle alors d'image RVB. Elles sont constituées, aprés prétraitement, de 3 canaux: le R, le V et le B. C'est le résultat de l'utilisation de la matrice de bayer.
- en noir et blanc. Il n'y a alors qu'un seul canal, représentant une nuance de gris.

{{<figure src="bayer-mask1.png" link="bayer-mask1.png" caption="Matrice de bayer d'une image couleur.">}}

Ce qu'il faut bien garder à l'esprit,c'est qu'une image RVB et une image Mono ne sont plus directement compatibles dans les formules de par leur structure.

## PixelMath
- Le fonctionnement de PixelMath est indépendant de tout autre process en cours de réalisation, mais aussi de toute image ou séquence précédemment chargée.
- Cependant, lorsque l'opération désirée est appliquée, le résultat est directement affiché dans la fenêtre de prévisualisation, prêt à être sauvegardé. Le nom proposé par défaut est 'nouvelle image vide'. 
- PixelMath ne gère que le format FITS. C'est un choix. Il est donc inutile d'essayer de charger directement des TIFF par exemple. En revanche, Siril sait très bien lire ce dernier format, la solution est donc d'ouvrir le fichier TIFF, de le sauvegarder en FITS, de le PixelMath-iser, puis de sauver le résultat au format désiré.
- Dans les formules applicables par Pixel Math, il faut garder à l'esprit que les images sont des **variables** comprises entre 0 et 1. **Elles ne sont pas normalisées à 2<sup>n</sup>-1**.
- Le nombre maximal d'images utilisables simultanément est fixé à 10. Vous allez voir que c'est déjà bien.
- Les images utilisables par PixelMath peuvent être linéaires ou non. Cependant, pour jouer **en live** avec les différentes couches, la version linéaire sera plus adéquate au niveau visuel.


## Interface graphique
Voici une vue globale de l'interface:

{{<figure src="PMwindow.png" link="PMwindow.png" caption="Vue de l'interface PixelMath">}}


- **zone 1**: Il s'agit ici de la composition de notre image, en 1 ou 3 canaux. Si l'image est au format RVB, seul le champ du haut (RVB/K) est accessible. Pour cela, la case "Utiliser une seule expression RVB/K" doit obligatoirement être cochée (sinon, un message d'erreur est affiché).
- **zone 2**: Permet d'ajouter une (ou des images) ou de les retirer de la liste. Le `+` ouvre une fenêtre standard où la sélection multiple de fichiers est possible. Le `-` supprime de la liste la variable en surbrillance. Il est indispensable que les images à ajouter soient du mêmes types (nombre de canaux) et de mêmes dimensions (LxW en pixel). Un message d'erreur est affiché sinon.
- **zone 3**: Liste des images utilisables, divisée en `Variables` (nom de variable modifiable par double clic) et `Chemin` (emplacement du fichier).
- **zone 4**: L'ensemble des fonctions utilisables est décrit plus loin.
- **zone 5**: L'ensemble des opérateurs aussi décrit plus loin.
- **zone 6**: Champs réservé aux paramètres (si utilisés). Voir plus loin.


## Les fonctions et opérateurs
Les diverses fonctions sont quasiment toute issues de la librairie standard.
Les opérateurs peuvent être soit algébriques, soit logiques.

{{<figure src="functions-fr.png" link="functions-fr.png" caption="Les différentes fonctions et opérateurs. Une version PDF est disponible [ici](function-Fr.pdf).">}}


## La fonction spéciale "iif"
Cette fonction de test a la structure suivante:
```
iif(condition, expression_si_Vrai, expression_si_Faux)
```

- **condition** est une expression logique portant sur un paramètre ou une variable.
- **expression_si_Vrai** est alors le résultat retourné si le test est positif.
- **expression_si_Faux** est alors le résultat retourné si le test est négatif.

Par exemple:

Prenons `myPict1`, une image 3 canaux (RVB donc).

{{<figure src="stat_myPic1.png" link="stat_myPic1.png" caption="Statistiques pour l'image RVB myPic1.fits">}}


On a ici R=[772.6, 65535], V=[977.5, 65535] et B=[698.3, 65535] (car l'image est en 16bit ici, donc max=65535).

Ou bien, écrit autrement, R=[0.018, 1], V=[0.015, 1] et B=[0.010, 1] (normalisé à 1).

En appliquant la condition suivante:

```
iif(myPic1>0.2, myPc1, 0.1)
```
cela revient à clipper à 6553.5 (=65535 x 0.1) les pixels de l'image `myPic1` dont la valeur est inférieure 13107 (=65535 x 0.2).
Pour les pixels dont la valeur est supérieure à 13107, leur valeur reste inchangée : ce que l'on voit très bien sur les nouvelles valeurs des statistiques.

A noter que ce raisonnement est vrai pour les 3 canaux **simultanément**.

{{<figure src="stat_myPic1_trunc.png" link="stat_myPic1_trunc.png" caption="Statistiques pour l'image RVB `myPic1.fits` aprés PM">}}


## Nom des variables
Par défaut, les variables sont nommées I1 à I10, mais il est tout à fait possible de personnaliser ces noms, en double cliquant dessus. Certaines règles doivent toutefois être respectées:
- chaînes de caractères autorisées
- chaînes alphanumériques autorisées
- ne peut pas contenir que des chiffres
- pas de caractères spéciaux, même au début, excepté pour `_`
- pas d'espace
- sensible à la casse

Voici un aperçu des combinaisons possibles:

{{<figure src="valid_var.png" link="valid_var.png" caption="Differentes syntaxes de variables autorisées">}}


## Bien plus important : nommage automatique
Dans certaines conditions, le nom de chaque variable peut être automatiquement attribué de façon cohérente, pour faciliter l'identification et l'utilisation de chaque variable. Ceci est possible grâce à l'utilisation du champ `FILTER=` dans l'entête du fichier FITS.

{{<figure src="fits_header_filter.png" link="fits_header_filter.png" caption="Le champ `FILTER` dans le HEADER">}}

Il y a plusieurs façons pour que ce champ soit automatiquement rempli :
- avec un paramétrage correct de votre logiciel d'acquisition (NINA, APT, ...).
Dans ce cas, les valeurs du header seront conservées au fur et à mesure des opérations de conversion/preprocessDOF/alignement/empilement.
On peut ainsi trouver des valeurs comme CLS, LX pour les images couleurs, ou OIII, Ha, SII pour du mono. Il est fortement déconseillé d'utiliser tout caractère spéciale, même si votre logiciel le permet. Cependant, le caractère spécial `_` est autorisé.

{{<figure src="filters_nina.png" link="filters_nina.png" caption="Le champ `FILTER` dans le HEADER après NINA">}}


- Lors d'une extraction RVB, HaOIII, Ha ou OIII réalisée par Siril à partir d'une image couleur (OSC). Siril rempli alors le champ `FILTER` du header avec la valeur adéquate.

{{<figure src="filters_extractHAOIII.png" link="filters_extractHAOIII.png" caption="Le champ `FILTER` dans le HEADER aprés un `extractHaOII`">}}

Voici en images un exemple:

- Une premiere image, dont le champ FILTRE est vide, subit une extraction RVB sous Siril. 
- On obtient alors 3 fichiers mono que l'on appelle `r.fits`, `g.fits` et `b.fits`.
- Une seconde image, dont le champ FILTRE est `FILTER=LX` (par exemple pour L-Extreme), subit aussi une extraction RVB sous Siril. 
- On obtient alors 3 fichiers mono que l'on appelle `rr.fits`, `gg.fits` et `bb.fits`.

On importe alors les 6 fichiers mono pour comparer l'auto-naming.

La suite en vidéo.

<video controls width=100%>
  <source src="extract_filter.webm" type="video/webm">
Your browser does not support the video tag.
</video> 

## Les paramètres

Tout comme en maths, ce sont des valeurs entières ou décimales, définies dans la **zone 5**.
Ces paramètres peuvent être utilisés indifféremment dans n'importe quelle formule (RVB ou mono), ajoutant alors une souplesse à l'utilisation des formules.

Il s'agit ici de lister les paramètres utilisables dans les diverses équations.

Cette liste peut être de la forme:
```
k=0.7, P=3 
```

La syntaxe:

- Les différents paramètres sont séparés par une virgule
- Le séparateur décimal est un point
- Chaînes de caractères alphanumériques, minuscules ou majuscules, incluant n'importe quels caractères spéciaux sauf `,` et `=`.
- Sensible à la casse

## Quelques Utilisations

Vous avez maintenant tous les outils pour utiliser PixelMath.
Place à l'action avec quelques exemples:

### Basiques

- Exemple 1 : manip d'images RVB

Dans cet exemple simple, il s'agit d'importer une image RVB dans PixelMath, de lui appliquer un facteur et de vérifier le résultat.

<video controls width=100%>
  <source src="RGB_example.webm" type="video/webm">
Your browser does not support the video tag.
</video> 

- Exemple 2 : Manip d'images Mono

Exemple similaire au précédant, mais avec un jeu d'images R, V et B. On ajoute en plus l'application d'un paramètre.

La formule appliquée, `k*R+~k*B`, permet de générer une **couche V synthétique** après l'utilisation d'un filtre Dualband, par exemple.

<video controls width=100%>
  <source src="Mono_example.webm" type="video/webm">
Your browser does not support the video tag.
</video> 

- Exemple 3 : Starless

Supposons que nous ayons une image RVB, ainsi que la version starless. Il est alors facile de générer le fichier "Stars".

<video controls width=100%>
  <source src="starless.webm" type="video/webm">
Your browser does not support the video tag.
</video> 

### Avancées

- Exemple 4: Générer un pseudo masque (mono)

Dans cet exemple, on part d'une image RVB. Après extraction des canaux R, V et B, on va fabriquer le masque à partir du canal R en jouant sur son histogramme (le noir doit être très noir, et le blanc doit rester dégradé).

- Une partie du masque est supprimée pour le rendre sélectif.
- Le masque, qui est en mono, est appliqué via PixelMath aux 3 couches R, V et B, de façon à faire "monter" le rouge d'une partie de l'image.
- L'image "locale" résultante est alors additionnée à l'image d'origine.

<video controls width=100%>
  <source src="advanced.webm" type="video/webm">
Your browser does not support the video tag.
</video> 


A vous de jouer maintenant!!!

---
title: Composition RVB
author: Vincent Hourdin
show_author: true
featured_image: RGB_composition2.fr.png
rate: "2/5"
type: page
---

Il peut être difficile de composer une image couleur à partir d'images réalisées avec différents filtres. Dans ce tutoriel, nous montrons quelques outils qui aident à gérer cela dans Siril, avec trois images prises avec des filtres S-II, H-alpha et O-III, mais la technique reste très similaire pour faire du RVB, LRVB ou toute autre combinaison d'images monochromes.

Pour commencer ce tutoriel, nous considérons que toutes les images ont le même échantillonnage et les mêmes dimensions, qu'elles ont été traitées indépendamment, que le gradient du fond de ciel est déjà supprimé et que leur histogramme a été étiré. Cela nous permet de **travailler uniquement avec le mode de rendu linéaire**, défini en bas de la fenêtre de l'image. Les images ne doivent pas avoir été recadrées afin de conserver les mêmes dimensions. Cela devrait ressembler à ceci dans Siril, avec le panneau de contrôle masqué (cliquez sur la barre verticale à l'extrême droite de l'application) :
{{<figure src="O-III.fr.png" caption="Image O-III" width="70%" >}}

{{<figure src="input_images.png" caption="Les images d'entrée" link="input_images.png" >}}

Comme il est très difficile d'obtenir la même luminosité pour toutes les images, une première étape consiste à d'utiliser l'outil `Linear Match` pour aligner les niveaux les uns avec les autres.

## Alignement des niveaux : Linear Match
Ouvrez vos images et comparez leur luminosité globale: certaines sont plus sombres, d'autres plus lumineuses. Choisissez celle que vous préférez, elle servira de référence pour aligner les autres. Puis ouvrez l'une des autres dans Siril pour commencer.

Ouvrez l'outil `Linear Match`, qui se trouve dans le menu `Traitement d'image` en bas de la liste (avant-dernier). Choisissez l'image de référence que vous avez choisie en y naviguant après avoir cliqué sur le bouton en haut de la fenêtre.

Cliquez ensuite sur `Appliquer`, vous devriez voir un changement dans l'image chargée, fermer ensuite la fenêtre et **enregistrer** l'image. Notez que l'utilisation du bouton ou du raccourci `Enregistrer` écrasera l'image d'entrée, il y a aussi un bouton `Enregistrer sous` à droite du bouton `Enregistrer` si vous souhaitez changer le nom de l'image modifiée.
{{<figure src="linear_match.fr.png" caption="La fenêtre de l'outil linear match, avec une image O-III chargée comme référence" >}}

Faites de même pour toutes les images sauf celle choisie comme référence et n'oubliez pas de toutes les sauvegarder après la manipulation et avant d'en ouvrir une nouvelle. Voici le résultat :
{{<figure src="lm_images.png" caption="Les images après application de l'outil Linear Match" link="lm_images.png" >}}

Nous pouvons voir que le fond de ciel est encore plus différent, mais l'important est que les nébuleuses ont une luminosité similaire dans les trois images. Le fond de ciel peut être corrigé après la composition des couleurs.

## La composition RVB : Créer une image couleur

Ouvrez maintenant l'outil de composition RVB, disponible en dernière entrée du menu `Traitement d'image`. Chaque ligne de la partie supérieure, où `non chargé` est écrit, représente une image d'entrée. Chacune d'elles se verra attribuer une couleur qui la fera contribuer à l'image couleur finale avec une teinte.

Chargez l'une des images en cliquant sur le bouton de navigation, à gauche de l'étiquette `non chargé`. Il peut s'agir de n'importe laquelle des images, car nous pourrons leur réaffecter une couleur cible plus tard. Notez le cas particulier de la première ligne qui est utilisée dans les cas où il y a une image de luminance. Ne chargez pas d'image ici si vous ne prévoyez pas d'utiliser la luminance dans la composition. Lorsque la première image est chargée, l'image couleur finale est créée et il est conseillé de sélectionner l'onglet RVB pour visualiser le résultat au fur et à mesure. Cela devrait ressembler à ceci :

{{<figure src="RGB_composition1.fr.png" caption="L'outil de composition RVB avec une première image chargée" >}}

Faites de même pour les deux autres images (ou plus). Pour faire apparaître une nouvelle ligne, si besoin, cliquez sur le gros bouton `+` à gauche. Dans notre cas, nous n'avons que trois images et nous les avons chargées dans l'ordre de la palette Hubble :
{{<figure src="RGB_composition2.fr.png" caption="L'outil de composition RVB avec 3 images chargées" >}}

On voit que les étoiles et le fond de ciel sont majoritairement magenta. Il est en effet difficile d'obtenir une couleur cohérente dès l'ajout des images pour un certain nombre de raisons :
- Les différents filtres peuvent ne pas être tous aussi larges en termes de bande passante, alors peut-être qu'ils ne laissent pas passer la même quantité de photons
- Le capteur n'est pas aussi sensible à toutes les longueurs d'onde, transformant les photons de différentes longueurs d'onde en plus ou moins d'électrons, rendant une image plus lumineuse que l'autre (pas le cas pour S-II et H-alpha qui sont très proches dans le rouge) ; cette situation devrait être améliorée par le `Linear Match` exécuté précédemment, mais ce n'est pas parfait
- Aucune nébuleuse, galaxie ou autre objet n'a la même luminosité dans toutes les bandes de filtre, donc certaines images sont plus lumineuses que d'autres; cela devrait également être amélioré par le `Linear Match` effectué précédemment
- L'histogramme n'a pas été étiré de la même manière pour toutes les images; cela devrait également être amélioré par le `Linear Match` effectué précédemment
- Les acquisitions pour chaque filtre n'ont pas été faites en même temps, donc certains d'entre eux auront eu un meilleur ciel (plus sombre ou avec une meilleure vue) que d'autres. Cela a un impact important sur la couleur des étoiles, car des étoiles plus grosses dans une image entraîneront des halos d'étoiles plus colorées (cela se produit dans l'image ci-dessus), à moins qu'une composition basée sur la luminance ne soit faite. C'est en effet l'un des avantages majeur d'utiliser une luminance, c'est cette image qui définit la netteté finale, pas les images utilisées pour la couleur.

**Remarque pour la teinte magenta**: la correction d'une teinte magenta comme celle-ci peut être effectuée avec l'outil de supression de bruit vert (`Supprimer le bruit vert` dans le menu de traitement d'image), après avoir mis l'image en négatif (peut être fait avec `contrôle` + `i`). En effet, une teinte magenta révèle une moindre contribution de vert, donc en négatif c'est le contraire. C'est un moyen simple d'améliorer la balance des blancs avec ce type d'image, mais ne fonctionne qu'avec une teinte magenta et après la fermeture de l'outil de composition RVB.

**Note pour la luminance**: lors d'une composition basée sur la luminance, en chargeant une image de luminance (même si elle n'est pas prise avec un filtre de luminance, cela peut parfois donner de bons résultats), la coloration de l'image peut se faire à l'aide de trois formules qui peuvent être choisies dans la liste à droite de la fenêtre où `composition TSL` est écrit.

**Remarque sur le binning et les dimensions des images**: la première image chargée détermine la taille de l'image de sortie. Si vous avez des images de tailles différentes, vous devez toujours charger la plus grande en premier. Si vos images sont différentes simplement à cause du bining, donc avec le même champ de vision, l'outil de composition met à l'échelle les images plus petites lorsqu'elles sont chargées pour correspondre à la taille de la première image chargée. C'est généralement utile pour le L-RVB pris avec les filtres de couleur en bin 2. Cela signifie également que si deux images n'ont pas été prises avec le même capteur, il est peu probable qu'elles aient le même champ de vision et le même échantillonnage de pixels après l'image rééchantillonnage, et cela ne fonctionnera pas avec cet outil.

### Choisir la couleur pour chaque image
Maintenant, la tâche difficile de choisir une couleur qui correspond à chaque image commence. Si vous avez chargé vos images dans un ordre aléatoire ou si vous n'aimez tout simplement pas le résultat, vous pouvez modifier la couleur attribuée à chaque image d'entrée. Cliquez sur la case colorée à gauche de chaque ligne, cela ouvrira un sélecteur de couleur.

{{<figure src="color_selector.fr.png" caption="Le selecteur de couleurs apparaît après avoir cliqué sur une couleur de l'outil composition RVB" >}}

Cette fenêtre contient 12 couleurs prédéfinies: rouge, vert, bleu, cyan (un mélange de bleu et de vert), magenta (un mélange de rouge et de bleu) et jaune (un mélange de rouge et de vert). Alors oui, cela fait 6. Mais ces dernières sont également dupliqués dans une version 50% plus sombre.

Lors de la sélection de la version lumineuse de ces couleurs, par exemple le rouge vif, l'image associée sera copiée en tant que canal rouge de l'image résultante. Si plusieurs images d'entrée doivent contribuer au canal rouge, il ne sera alors pas une bonne idée de les rendre toutes rouges à 100%, sinon le canal rouge du résultat sera trop lumineux et sera totalement saturé dans les zones les plus lumineuses. Par exemple, la composition d'une image R + G + B + H-alpha avec le H-alpha attribué au rouge nécessitera un mélange entre l'image de filtre rouge et l'image de filtre H-alpha pour le canal rouge de l'image de sortie. Cela peut être fait simplement à 50%-50% en sélectionnant un rouge deux fois moins lumineux dans cette fenêtre pour ces deux images.

Pour illustrer cela, j'ai choisi ici le cyan pur, le magenta pur et le jaune pur pour les trois images, donc chacun des canaux de couleur de l'image de sortie (rouge, vert et bleu) a deux images en entrée qui contribuent à pleine puissance. Le résultat est alors trop lumineux, les étoiles sont saturées.
{{<figure src="RGB_composition_CMJ1.fr.png" caption="Une coloration alternative mais trop lumineux" >}}

La bonne façon de faire aurait été ici de sélectionner les demi-couleurs de l'outil, si vraiment il s'agissait des teintes désirées pour chaque image. En fait, après avoir sélectionné les couleurs de pleine luminosité comme ci-dessus, cliquer sur le bouton `Ajuster la luminosité des calques` le fera automatiquement, mais encore mieux : si les images d'entrée n'utilisent pas la dynamique complète, ce qui est peu probable, l'outil le détectera et autorisera un peu plus de 50% de la luminosité à chacune. De cette façon, nous obtenons une image non saturée :
{{<figure src="RGB_composition_CMJ2.fr.png" caption="Une coloration alternative non saturée" >}}

### Sélection de couleurs personnalisées
On pourrait être tenté d'ajuster le rapport de puissance d'un canal en fonction des images d'entrée. Cela se fait actuellement en choisissant une couleur qui contient cette modulation de puissance. Par exemple, pour que l'image O-III contribue à 30% au vert et à 100% au bleu, la couleur associée à l'image devra être exactement cela.

Cliquez sur le bouton personnalisé `+` de la fenêtre de sélection de couleur que vous avez déjà vue. Cela changera la fenêtre en une palette de couleurs. Malheureusement, le sélecteur de couleur que Siril utilise actuellement est fourni de manière générique par la boîte à outils graphique qu'il utilise, et la convention est d'utiliser l'hexadécimal pour représenter les couleurs, sur 8 bits, donc sur 256 valeurs. Donc ici, pour obtenir la couleur verte à 30%, nous devons calculer 30% de 255 et le convertir en hexadécimal, cela devient `64`. De nombreux outils en ligne comme [celui-ci](https://www.joshuamiron.com/percent-to-hex-converter) peuvent vous aider. Dans `#0064ff`, nous avons deux zéros pour le rouge, 6 et 4 pour le vert et les deux f pour le bleu, `ff` signifie 255, ou 100%.

{{<figure src="color_selector2.fr.png" caption="L'outil personnalisé du sélecteur de couleurs utilise une représentation hexadécimale des couleurs" >}}

**Remarque** : il serait identique de définir une couleur rouge et bleue, donc magenta, sur une image ou d'ajouter l'image deux fois, une fois avec une couleur rouge et l'autre avec une couleur bleue.

Après avoir choisi les teintes que vous aimez et la luminosité correspondante à chaque canal, probablement après plusieurs itérations, vous pourrez continuer le traitement de votre image finale. Notez que la **balance des blancs n'a pas à être parfaite à ce stade**, car probablement une neutralisation de l'arrière-plan et des opérations de balance des blancs devront être effectuées, peut-être un ajustement de l'histogramme, n'oubliez pas le recadrage en premier...

{{<figure src="RGB_composition_custom.fr.png" caption="Une composition basée sur des couleurs personnalisées, pas forcément la meilleure solution pour cet ensemble d'images" >}}

De plus, il est possible d'aligner les images à partir de cet outil, en sélectionnant une étoile et en cliquant sur le bouton `Aligner` après avoir choisi le mode d'alignement sur une étoile. Cela ne corrigera cependant pas la rotation entre les images. Cela peut être fait en renommant tous vos fichiers d'entrée en un seul nom de base et en chargeant cette séquence puis en effectuant un alignement global de la séquence.


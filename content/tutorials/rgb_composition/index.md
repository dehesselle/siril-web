---
title: RGB composition
author: Vincent Hourdin
show_author: true
featured_image: RGB_composition2.png
rate: "2/5"
type: page
---

It can be tricky to compose a colour image from images made with various filters. In this tutorial, we will show some tools that help managing this in Siril, with three images taken with S-II, H-alpha and O-III filters, but it would be very similar for RGB or LRGB or any combination of monochrome images.

As an input of this tutorial, we consider that all images have the same sampling and dimensions, that they have been processed independently and that they have their background gradient already removed and that their histogram has been stretched. This will allow us to **work only with the Linear rendering mode**, set at the bottom of the image window area. They must not have been cropped to keep them the same dimensions. That should look like this in Siril, with the control panel hidden (click the vertical bar on the extreme right):
{{< figure src="O-III.png" caption="O-III image" width="70%" >}}

{{< figure src="input_images.png" caption="The input images" link="input_images.png" >}}

Since it will be very hard to have the same brightness in all input images, a first step will be to use the linear matching tool to align their levels against each other.

## Levels alignment: The linear match
Open your images and compare their overall brightness: some are darker, some brighter. Peak one you like, it will be used as a reference to align the others. Then open one of the others.

Open the linear match tool, found in the `Image processing` menu at the bottom of the list (second to last). Choose the reference image that you chose by browsing to it after having clicked on the button at the top of the window.

Then click on Apply, you should see a change in the image, close the window and **save** the image. Note that using the save button or shortcut will overwrite the input image, there is also a `Save as` button right to the `Save` button if you want to change the name of the modified image.
{{< figure src="linear_match.png" caption="The linear match tool window, with the O-III image selected as reference" >}}

Do the same for all input images except the one chosen for calibration, don't forget to save all of them before opening a new one. Here is the result:
{{< figure src="lm_images.png" caption="The linear-matched images" link="lm_images.png" >}}

We can see that the background is even more different, but the important part is that the nebulae is of similar brightness in the three images. The background will be corrected after the colour composition.

## The RGB composition: Creating a colour image

Now open the RGB composition tool, available as the last entry of the `Image processing` menu. Each line of the top part, where `not loaded` is written, will represent one input image. Each of them will be assigned a colour that will make it contribute to the final colour image with a tint.

Load one of the images by clicking on the browsing button, left of the `not loaded` label. It can be any of the images, as we will be able to reassign them a target colour later. Note that the first line is used in cases where there is a luminance image in the set, just don't load one here if you don't plan to use luminance in the composition. When the first image is loaded, the final colour image is created and I suggest you select the RGB tab to visualize the result already. This should look like this:

{{< figure src="RGB_composition1.png" caption="The RGB composition tool with the first image loaded" >}}

Do the same for the two (or more) other images. To make new line appear if needed, click on the big `+` button on the left. In our case, we have just three images and we have loaded them in the order of the Hubble palette:
{{< figure src="RGB_composition2.png" caption="The RGB composition tool with the three images loaded" >}}

We can see that the stars and the background sky are mostly magenta. It's indeed hard to get a consistent colour right from the addition of the images for a number of reasons:
- The different filters may not be all as wide in terms of bandwidth, so maybe they don't let the same amount of photons go through
- The sensor is not as sensitive for all wavelength, transforming the photons of different wavelengths to more or less electrons, making one image brighter than the other (not the case for S-II and H-alpha which are very close in the red); this should be improved by the linear match done previously, but it's not perfect
- No nebula, galaxy or other object has the same brightness in all filter bands, so some images are brighter than others; this should also be improved by the linear match done previously
- The histogram has not been stretched in the same way for all images; this should also be improved by the linear match done previously
- The acquisitions for each filter have not been done at the same time, so some of them will have had a better sky (darker or with a better seeing) than others. This impacts star colour a lot, because bigger stars in one image will result in more coloured star rims (this happens in the image above), unless a luminance-based composition is made. This is in fact one of the great advantages of using a Luminance layer, which has the highest quality, the colour-contributing images would not redefine the sharpness of the image.

**Note for magenta tint**: correcting for a magenta tint like that can be done with the green dominant tint removal (`Remove green noise` in the image processing menu), after having put the image in negative (can be done with `control` + `i`). Indeed, a magenta tint reveals a lower contribution of green, so in negative it's the opposite. It's a simple way to improve the white balance with this kind of image, but only works with magenta tint, and after the RGB composition tool has been closed.

**Note for luminance**: when activating the luminance-based composition, by loading a luminance image (even if it's not taken with a luminance filter, sometimes it can give good results), colouring of the image can be done using three formulas. They can be chosen from the list on the right of the window where `HSL composition` is written.

**Note for binning and image dimensions**: the first loaded image determines the size of the output image. If you have images of different sizes, you should always load the largest first. If your images are different just because of bining, so with the same field of view, the composition tool will upscale the smaller images when they are loaded to match the size of the first loaded image. It is useful for the common L-RGB taken with the colour filters in bin 2. This also means that if two images have not been taken with the same sensor, it is unlikely they will have the same field of view and pixel sampling after image resampling, and this will not work with this tool.

### Choosing colours for each image
Now the hard task of choosing a colour that fits each image begins. If you have loaded your images in a random order or if you just don't like the result, you can change the colour assigned to each input image. Click on the coloured box on the left of each line, this will open a colour chooser.
{{< figure src="color_selector.png" caption="The colour selector appears when clicking on a coloured box from the RGB composition window" >}}

There are 12 predefined colours in it: red, green, blue, cyan (a mix of blue and green), magenta (a mix of red and blue) and yellow (a mix of red and green). Yes that's 6. But they are duplicated to their 50% darker version too.

When selecing the bright version of these colours, for example the bright red, the associated image will be copied as the red channel of the output image. If several input images need to contribute to the red channel of the output image, then it will be wise to not make them all 100% red, otherwise the result's red channel will be too bright and will even overflow on stars or other high value pixels of the input image. For example, composing a R+G+B+H-alpha image with the H-alpha assigned to the red will require a mix between the red filter image and the H-alpha filter image for the red channel of the output image. It can be done simply 50%-50% by selecting the half power red in this window for these two images.

To illustrate this, I chose here the full cyan, full magenta and full yellow for our three images, so each of the colour channels of the output image (red, green and blue) have two contributing input images at full power. The result is too bright, stars are saturated.
{{< figure src="RGB_composition_CMJ1.png" caption="An alternate but too bright colouring" >}}

The correct way would have been here to select the half colours from the tool, if really that was the hues wanted for each image. In fact, after having selected the full brightness colours as above, clicking on the `Adjust layers brightness` button will do that automatically, but even better in fact: if the input images do not use the full dynamic, which is unlikely, the tool will detect it and allow a bit more than the 50% of the brightness to each. That way, we get a non-saturated image: 
{{< figure src="RGB_composition_CMJ2.png" caption="An alternate non-saturated colouring" >}}

### Selecting custom colours
One might be tempted to adjust the ratio of power for a channel depending on the input images. This is done currently by choosing a colour that contains this power modulation. For example, to make the O-III image contribute 30% to the green and 100% to the blue, the colour associated to the image will have to be exactly that.

Click on the custom `+` button of the colour chooser window you already saw. This will change the window to a colour palette. Unfortunately, the colour chooser that Siril currently uses is generically provided by the graphical toolkit it uses, and the convention is to use hexadecimal for that, on 8 bits, so on 256 values. So here, to get my 30% green colour, I have to compute 30% of 255 and convert that to hexadecimal, that becomes `64`. Many Web design online tools like [this one](https://www.joshuamiron.com/percent-to-hex-converter) can help you with that. In `#0064ff`, we have two zeros for the red, 6 and 4 for the green and the two f for the blue, `ff` means 255, or 100%.

{{< figure src="color_selector2.png" caption="The colour selector custom tool uses hexadecimal representation of colours" >}}

**Note**: it is identical to set a red and blue, so magenta, colour to an image and adding the image twice, once with a red colour and the other with a blue colour.

After having chosen the hues that you like and the corresponding power to each channel, probably after several iterations, you can get continue the processing of your final image. Note that the **white balance doesn't have to be perfect at this step**, since probably a background neutralization and white balance operations should be done, maybe a histogram adjustment, don't forget the cropping first...

{{< figure src="RGB_composition_custom.png" caption="A composition based on custom colours, not necessarily the best for this set of images" >}}

Also, it is possible to align the images from this tool, by selecting a star and clicking on the `Align` button after having chosen the one-star alignment mode. This will not correct rotation between images. It can be done by renaming all your input files to a single base name and loading this sequence and doing a registration on them.


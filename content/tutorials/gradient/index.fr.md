---
title: Supression de gradients
author: Cyril Richard
show_author: true
featured_image: gradient.png
rate: "1/5"
type: page
---

{{< table_of_contents >}}

Dans ce tutoriel, qui s'adresse à tous les niveaux, nous allons aborder le traitement des gradients présents dans les images astronomiques. En effet, même lorsque les images sont prises sous un ciel chilien, et c'est le cas pour l'image qui sert d'illustration, celles-ci peuvent être entachées par un gradient lumineux dont l'origine peut varier.

{{<figure src="gradient.png" link="gradient.png" caption="Un exemple de gradient lumineux, dans l'angle en haut à droite, sur une image prise sous un ciel du Chili.">}}

Que ce soit la pollution lumineuse de la ville voisine, de la Lune ou de tout autre source lumineuse, ces gradients sont des signaux indésirables qu'il est souhaitable de supprimer. Attention cependant, il est important de faire la distinction entre un gradient lumineux, dont l'origine est une source de lumière externe, et le vignettage dû à l'optique. Ce dernier, bien qu'on puisse l'atténuer avec un outil de retrait de gradient, n'est parfaitement corrigé qu'avec l'application d'images flats.

Enfin, ce tutoriel s'adresse aux utilisateurs possédant au moins la version 1.0.2 de Siril pour laquelle l'outil a été entièrement revu, corrigé et amélioré.

## Suppression d'un gradient sur une image empilée

Dans la plupart des cas, le problème du retrait du gradient se pose juste après l'empilement, lorsque l'addition des pixels fait ressortir le signal du bruit. De plus, le gradient n'est pas homogène d'une image à l'autre et le résultat, qui est la somme de tous les gradients individuels, apparaît plus visible.

### Présentation de l'outil
L'outil `Extraction de Gradient` possède deux méthodes d'interpolation s'utilisant à peu près de la même manière. La méthode polynomiale, et la méthode RBF, signifiant Radial Basis Function. C'est cette dernière dont nous allons parler dans cette section car elle permet de traiter des gradients plus complexes en utilisant peu d'échantillons. La fenêtre de l'outil se présente comme suivant (on la trouve dans le menu *Traitement d'image* sous le nom *Extraction du gradient...*) :
{{<figure src="bkg_extract_dialog.fr.png" link="bkg_extract_dialog.fr.png" caption="Présentation de l'outil d'extraction de gradient de Siril.">}}

1. Il est possible de choisir deux méthodes d'interpolation. 
   - La première, existante depuis de nombreuses années, est l'interpolation polynomiale. Elle permet de construire des fonds de ciel synthétique et marche très bien pour des gradient assez simple. La complexité des gradients corrigés est déterminée par le degré de l'ordre du polynôme utilisé, le maximum étant de 4. Plus haut est le degré et plus complexe est le gradient. Un des avantages de cette méthode sera décrit dans la section suivante.
   - La deuxième méthode, l'interpolation RBF, est celle que nous allons présenter dans ce tutoriel, et celle que nous vous conseillons d'utiliser dans la majeur partie des cas.
1. Le lissage est un paramètre de l'interpolation RBF. Un paramètre de lissage trop petit peut entraîner des dépassements et des sous-dépassements entre les points de l'arrière-plan, tandis qu'un paramètre de lissage trop grand peut ne pas convenir aux grands écarts de gradient. Il est recommandé d'essayer plusieurs valeurs itérativement. La valeur par défaut est 0.5.
1. Il est possible de générer une grille de points, appelés échantillons, de façon automatique. Cette option définit le nombre d'échantillons qu'il y a sur une ligne. Plus le nombre est important et plus le temps de calcul est grand.
1. La tolérance de la grille est un paramètre définissant si oui ou non un échantillon est posé sur un endroit de l'image. En effet, il faut éviter de poser des échantillons sur des étoiles et / ou des objets de la photo n'appartenant pas au fond de ciel.
1. Parfois, lorsque la profondeur des couleurs est insuffisante pour échantillonner une gradation visuellement continue des couleurs, il est possible qu'apparaissent des cercles ou lignes colorée. On parle alors de postérisation. Cette option permet de générer un bruit aléatoire et de supprimer cet effet indésirable.
1. L'appui sur ce bouton génère une grille d'échantillon automatiquement, en tenant compte des paramètres abordés plus haut.
1. Ce bouton permet quant à lui d'effacer l'ensemble des échantillons présent sur l'image.
1. Il est possible de corriger le gradient de deux façons. Soit en soustrayant le fond de ciel synthétique (méthode généralement recommandée) soit en le divisant. Cette dernière peut être préférée dans le cas où on cherche à compenser un problème de flat.
1. Ce bouton applique la correction de gradient. Il est possible d'appuyer dessus autant de fois que l'on le désire, en modifiant au préalable les paramètres, pour corriger l'image de façon itérative.
1. Ces options permettent d'appliquer la correction de gradient à la séquence d'images entière telle que nous l'aborderont dans la section suivante.

### Utilisation
Le premier reflexe à avoir, et qui permet d'avoir une bonne vision du gradient présent dans l'image, est de se placer en mode de visualisation `Histogramme`. Ainsi, l'image est visuellement très étirée et tous les défauts sont parfaitement visibles. On peut aussi utiliser le mode `fausses couleurs` pour encore plus de clarté.

{{<figure src="autostretch.png" link="autostretch.png" caption="Une image avec un gradient particulièrement complexe vu en mode auto-ajustement.">}}
{{<figure src="histo.png" link="histo.png" caption="La même image affichée en mode égalisation histogramme.">}}
{{<figure src="falsecolor.png" link="falsecolor.png" caption="Cette fois, l'image en mode histogramme est affichée en fausses couleurs.">}}

Ensuite, il est possible de positionner les échantillons de deux façons. Soit manuellement, soit automatiquement tel que décrit plus haut. Pour les poser manuellement il suffit d'un clic gauche de la souris à l'endroit désiré de la photo. Un clic droit sur un échantillon permet au contraire de le supprimer. L'interpolation RBF ne nécessite pas la présence d'un grand nombre d'échantillons et c'est pourquoi la pose manuel est tout indiquée ici. On peut également combiner les deux méthodes, en posant une grille automatiquement puis en rajoutant/enlevant des échantillons. Quelques règles doivent cependant être respectées :
- On distribue les échantillons sur l'image entière, en prenant soin de les poser uniquement sur le fond du ciel.
- On évite de poser un échantillon sur une étoile.

{{<figure src="bkg_samples.fr.png" link="bkg_samples.fr.png" caption="Ici, la position des échantillons est choisie automatiquement afin d'avoir une grille compacte pour essayer de corriger le gradient.">}}
{{<figure src="bkg_samples_1st_try.fr.png" link="bkg_samples_1st_try.fr.png" caption="Un appui sur le bouton `Calculer le Gradient` permet d'obtenir un premier jet.">}}

Après avoir choisit les échantillons, un appui sur le bouton `Calculer le Gradient` affiche le résultat. Ce résultat est temporaire et il faut cliquer sur `Appliquer` pour le valider. Cependant, comme on peut le voir sur l'image donnée en exemple, même si la plus grosse partie du gradient a disparu, certains spots sombres sont encore visibles. Généralement, ceci est dû au fait que certains échantillons sont placés sur une étoile, ou autre objet de l'image. Il suffit alors de supprimer l'échantillon qui pose problème et de relancer le calcul. Dans cet exemple, le lissage est très bas afin de rendre l'effet encore plus fort, cependant il faut faire attention aux effets de bords qui sont alors plus importants.

{{<figure src="bkg_result.fr.png" link="bkg_result.fr.png" caption="Après plusieurs essais successifs, le résultat est le suivant.">}}

Après un travail itératif, l'image ci-dessus présente le résultat. Bien que non parfait, on voit que la majeure partie du gradient a été traitée. On peut donc appuyer sur `Appliquer`, ce qui fermera l'outil et nous ramènera à l'image traitée. Appuyer sur `Fermer` permet d'annuler les changements et donc revenir à l'image d'origine.

{{<figure src="bgk_result_autostretch.png" link="bgk_result_autostretch.png" caption="La même image vue en mode auto-ajustement, les défauts sont dorénavant très peu visibles.">}}

Ci-après, un exemple où on essaye de supprimer la pollution lumineuse. Seuls quelques échantillons posés manuellement sont alors nécessaires.

{{<figure src="IFN_or.png" link="IFN_or.png" caption="Image originale présentant un fort gradient dû à la polution lumineuse.">}}
{{<figure src="IFN_samples.fr.png" link="IFN_samples.fr.png" caption="La pose des échantillons est très simple et seuls quelques uns sont nécessaires (8 échantillons ont été utilisés dans cet exemple). Cliquez sur l'image pour voir plus en détail.">}}
{{<figure src="IFN_fin.png" link="IFN_fin.png" caption="Le résultat est totalement débarrassé du gradient et les IFN deviennent visibles.">}}

## Suppression d'un gradient sur les images d'une séquence
Il peut être parfois intéressant de traiter le gradient sur les images individuelles de la séquence. En effet, le gradient d'une image empilée est la somme de tous les gradients contenus dans chaque image. Par conséquent, il peut s'avérer que ce dernier soit très, trop, complexe pour être parfaitement traité.

Dans ce cas, l'interpolation polynomiale au degré le plus bas (degré 1) est tout indiquée pour corriger ces gradients simples, souvent linéaires.

{{<figure src="bkg_sequence_poly1.fr.png" link="bkg_sequence_poly1.fr.png" caption="Ici l'extraction du gradient est appliquée à chaque imagede la séquence.">}}

Il n'est pas nécesaire de cliquer sur `Calculer le Gradient` avant d'appliquer : la grille et le gradient seront calculés pour chaque image. Au final, après l'empilement, le gradient à corriger sera bien plus simple à traiter.


---
title: "Tutorials"
author: Cyril Richard
menu: "main"
weight: 4 
---



## Third Party
* [Processing a nightscape in Siril](https://pixls.us/articles/processing-a-nightscape-in-siril/)

## YouTube Channels
* [Official Siril channel](https://www.youtube.com/channel/UC_8UVzay-xlds4pjyRhHUOw)
* [L'astrophotographie à l'APN (french)](https://www.youtube.com/channel/UCL6_FyoBsia3FlnfdSDA0CA)

## Online Training
* https://www.learn.siril.org/
